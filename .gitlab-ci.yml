image: docker:20.10.16

stages:
  - build
  - linting
  - testing

variables:
  BACKEND_IMAGE_DEV: dima007/shop-backend-image:latest
  BACKEND_IMAGE_PROD: dima007/shop-backend-image:prod
  FRONTEND_IMAGE_DEV: dima007/shop-frontend-image:latest
  FRONTEND_IMAGE_PROD: dima007/shop-frontend-image:prod
  REDIS_URL: redis://redis:6379
  DB_USER: postgres
  DB_NAME: postgres_for_shop
  DB_PASSWORD: example
  DB_HOST: postgres-db
  POSTGRES_HOST_AUTH_METHOD: trust

services:
  - docker:20.10.16-dind

before_script:
  - echo "$DOCKERHUB_ACCESS_TOKEN" | docker login --username "dima007" --password-stdin
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'

build-backend-dev:
  stage: build
  script:
    - docker build --target development -t "$BACKEND_IMAGE_DEV" -f Dockerfile .
    - docker push "$BACKEND_IMAGE_DEV"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - .gitlab-ci.yml
        - Dockerfile
        - requirements.txt

build-frontend-dev:
  stage: build
  script:
    - docker build --target development -t "$FRONTEND_IMAGE_DEV" -f frontend/Dockerfile frontend
    - docker push "$FRONTEND_IMAGE_DEV"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - .gitlab-ci.yml
        - frontend/Dockerfile
        - frontend/package-lock.json

build-backend:
  stage: build
  script:
    - docker build --target production -t "$BACKEND_IMAGE_PROD" -f Dockerfile .
    - docker push "$BACKEND_IMAGE_PROD"
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

build-frontend:
  stage: build
  script:
    - docker build --target production -t "$FRONTEND_IMAGE_PROD" -f frontend/Dockerfile frontend
    - docker push "$FRONTEND_IMAGE_PROD"
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

lint-backend:
  stage: linting
  script:
    - docker pull $BACKEND_IMAGE_DEV
    - docker run --rm -v ${PWD}:/backend-app $BACKEND_IMAGE_DEV flake8 --jobs=1
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == "main"'

lint-frontend:
  stage: linting
  script:
    - docker pull $FRONTEND_IMAGE_DEV
    - docker run --rm -v ${PWD}/frontend:/frontend-app/frontend $FRONTEND_IMAGE_DEV npm run lint --prefix frontend
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == "main"'

test-backend:
  stage: testing
  image: $BACKEND_IMAGE_DEV
  variables:
    DB_HOST: postgres-db
    DB_USER: postgres
    DB_NAME: postgres_for_shop
    DB_PASSWORD: example
  services:
    - docker:20.10.16-dind
    - name: postgres:16.1
      alias: postgres-db
    - name: redis:5.0.1
      alias: redis
  before_script:
    - apt-get update && apt-get install -y docker.io
  script:
    - pytest && coverage xml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == "main"'

test-frontend:
  stage: testing
  image: $FRONTEND_IMAGE_DEV
  before_script:
    - apt-get update && apt-get install -y docker.io
  script:
    - cd frontend && npm install --only=dev
    - npm run test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == "main"'
