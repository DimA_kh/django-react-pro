# Django-React-Pro

## Getting started

For local development
---

### Requirements

Make sure [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
and [Docker Compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)
are installed on your computer.

### Cloning the Project

To clone the project, execute the following commands in the terminal:

```bash
git clone git@gitlab.com:DimA_kh/django-react-pro.git
cd django-react-pro
```

### How to Use

**Run the provision command, to configure the all services and create superuser:**

```bash
make provision
```

**Additional commands:**

1. Building frontend and backend Docker images for development without cache:

```bash
make build-frontend
make build-backend
```

2. Building all Docker Images:

```bash
# for development
make build-dev
# for production
make build-prod
```

3. Starting and stopping containers:

```bash
# in development mode with hot-reload
make start-dev
# in production mode
make start-prod
make stop
make restart
```

4. Executing database migrations:

```bash
make migrate
```

5. Creating a superuser:

```bash
make create-superuser
```

A super user will be created with:
    - email: **admin@mail.com**
    - password: **admin**

6. The project will be available on the ports:

- frontend `http://localhost:5173`
- backend `http://localhost:8000`

7. To enter the frontend or backend container, run:

```bash
make shell.frontend
make shell.backend
```

## Authors and acknowledgment
- [Alipov Dmytro](https://gitlab.com/DimA_kh)

## License
This project is licensed under the [MIT License](https://gitlab.com/DimA_kh/django-react-pro/-/blob/main/LICENSE).
