from django.contrib import admin
from django.urls import path, include

from shop.api_documentation import schema_view

urlpatterns = [
    path("api-docs/", schema_view.with_ui('swagger', cache_timeout=0), name='schema_swagger_ui'),
    path("redoc/", schema_view.with_ui('redoc', cache_timeout=0), name='schema_redoc'),

    path("admin/", admin.site.urls),
    path("seller-dashboard/", include('seller_dashboard.urls', namespace='seller_dashboard')),

    path("api/auth/", include('authentication.urls', namespace='auth')),
    path("api/shop/", include('core.urls')),
]
