from rest_framework import permissions
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view


class CustomSchemaGenerator(OpenAPISchemaGenerator):
    """
    Custom schema generator that allows filtering out specific API paths
    from the Swagger documentation. This class overrides the `get_paths`
    method to exclude certain paths from the generated schema.
    """

    def get_paths(self, endpoints, components, request, public):
        """
        Overrides the default method to filter out specific API paths from
        the generated schema.
        """
        paths, prefix = super().get_paths(endpoints, components, request, public)
        excluded_paths = [
            "/auth/refresh/",
            "/auth/verify/",
        ]
        # Filter out the excluded paths.
        filtered_paths = {
            path: path_item
            for path, path_item in paths.items()
            if path not in excluded_paths
        }
        return filtered_paths, prefix


# Configure the schema view to use the custom schema generator
schema_view = get_schema_view(
    openapi.Info(
        title="BEST SHOP API",
        default_version="v1",
        description="API Documentation for BEST SHOP",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="support@bestshop.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=False,
    permission_classes=[permissions.AllowAny],
    generator_class=CustomSchemaGenerator,
)
