# Stage 1: Install dependencies
FROM python:3.9-alpine AS builder

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache gcc musl-dev libffi-dev git

# Set the working directory to /backend-app
WORKDIR /backend-app

# Copy only requirements.txt to avoid re-installing dependencies
COPY requirements.txt .

# Install dependencies and clean up cache
RUN pip install --no-cache-dir -r requirements.txt && rm -rf /root/.cache/pip/*

# Stage 2: Development stage
FROM builder AS development

# Set the working directory to /backend-app
WORKDIR /backend-app

# Copy the application code
COPY . .

# Default command to be executed when running the container in development
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# Stage 3: Production stage
FROM builder AS production

# Set the working directory to /backend-app
WORKDIR /backend-app

# Copy the application code
COPY . .

# Install production dependencies
RUN pip install --no-cache-dir gunicorn

# Default command to be executed when running the container in production
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "shop.wsgi:application"]
