from datetime import timedelta
import logging

from apscheduler.schedulers.background import BackgroundScheduler
from django.utils import timezone
from dramatiq import actor
from core.models import Order

logger = logging.getLogger(__name__)


@actor
def cancel_old_orders():
    """
    Every half hour transfers orders with the status in_progress
    and older than 20 minutes to the cancelled state.
    """
    try:
        logger.info("Running cancel_old_orders task...")
        time_threshold = timezone.now() - timedelta(minutes=20)
        for order in Order.objects.filter(status=Order.IN_PROGRESS, created_at__lt=time_threshold):
            order.status = Order.CANCELED
            order.save()
    except Exception as e:
        logger.error(f"Error in cancel_old_orders: {e}")


scheduler = BackgroundScheduler()
scheduler.add_job(cancel_old_orders.send, 'cron', minute='*/30')
scheduler.start()
