from django.conf import settings
import meilisearch


class MeiliSearchClient:
    """
    Client for interacting with MeiliSearch.
    """

    def __init__(self):
        """
        Initializes the MeiliSearch client using settings from Django.
        """
        self.client = meilisearch.Client(settings.MEILI_HOST, settings.MEILI_MASTER_KEY)

    def search(self, index_name, query):
        """
        Searches an index in MeiliSearch.

        Args:
            index_name (str): The name of the index.
            query (str): The search query.

        Returns:
            dict: Search results, including documents and metadata.
        """
        index = self.client.index(index_name)
        return index.search(query)

    def add_documents(self, index_name, documents):
        """
        Adds documents to an index in MeiliSearch.

        Args:
            index_name (str): The index name.
            documents (list): The list of documents to add.
        """
        index = self.client.index(index_name)
        index.add_documents(documents)
