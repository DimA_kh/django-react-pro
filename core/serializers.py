from rest_framework import serializers
from core.models import Order, Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'id',
            'title',
            'image',
            'price',
            'quantity',
            'description',
        )


class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
            'id',
            'total',
            'status',
            'delivery_address',
            'delivery_date',
            'delivery_time',
            'payment_at',
            'created_at',
            'phone',
        )
