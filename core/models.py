from django.db import models
from django.utils import timezone
from jsonfield.fields import JSONField


class Product(models.Model):
    title = models.CharField(max_length=255, unique=True)
    image = models.CharField(max_length=255, blank=True, null=True)
    price = models.IntegerField(default=1)
    quantity = models.IntegerField(default=0)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.title


class Order(models.Model):
    IN_PROGRESS = "in_progress"
    PAID = "paid"
    COMPLETED = "completed"
    CANCELED = "canceled"

    ORDER_STATUS = [
        (IN_PROGRESS, "In Progress"),
        (PAID, "Paid"),
        (COMPLETED, "Completed"),
        (CANCELED, "Canceled"),
    ]

    id = models.CharField(primary_key=True, max_length=65, unique=True)
    buyer = models.ForeignKey("authentication.User", on_delete=models.SET_NULL, null=True)
    buyer_email = models.EmailField()
    buyer_name = models.CharField(max_length=100, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    delivery_address = models.CharField(max_length=255, blank=True, null=True)
    delivery_date = models.DateField(blank=True, null=True)
    delivery_time = models.TimeField(blank=True, null=True)
    total = models.SmallIntegerField(blank=True, null=True)
    status = models.CharField(max_length=20, choices=ORDER_STATUS, default=IN_PROGRESS)
    products = JSONField(default=list)
    payment_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"User: {self.buyer_email}, Order: {self.id[:8]}"

    def save(self, *args, **kwargs):
        """
        Handler order status change and update product quantities.

        This method intercepts the save operation for an Order instance.
        When an existing order is saved with a change in its 'status' field
        to 'CANCELED', it triggers an update to the product quantities to
        revert the reserved quantities for canceled items.
        """
        if self.pk:
            try:
                original_order = Order.objects.get(id=self.pk)
                if original_order.status != self.status and self.status == Order.CANCELED:
                    self._update_product_quantity(original_order.products)
            except:
                pass
        super().save(*args, **kwargs)

    def _update_product_quantity(self, products):
        """
        Updates the quantity of products after order cancellation.
        """
        product_ids = [item["id"] for item in products]
        products_amount = {item["id"]: item["amount"] for item in products}
        products_to_update = []
        for product in Product.objects.filter(id__in=product_ids):
            products_to_update.append(Product(
                id=product.id,
                quantity=product.quantity + products_amount[product.id]
            ))
        Product.objects.bulk_update(products_to_update, ["quantity"])
