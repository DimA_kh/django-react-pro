import time
from django.conf import settings
from django.core.cache import cache
from rest_framework.throttling import BaseThrottle


class IPBasedThrottleCore(BaseThrottle):
    """
    Custom IP-based throttling class.
    """
    def __init__(self):
        self.key = None
        self.history = None

    def get_cache_key(self, request):
        """
        Generates a cache key based on the user's IP address.

        Returns (str): The cache key for the user's IP address.
        """
        # Using the user's IP address to restrict
        return f"throttle_ip_{self.get_ident(request)}"

    def allow_request(self, request, view):
        """
        Determines if the request should be allowed based on the IP-based rate limit.
        """
        self.key = self.get_cache_key(request)
        if not self.key:
            # If you can't get an IP, don't limit it
            return True
        self.history = cache.get(self.key, [])
        now = time.time()
        # Delete old records (requests that happened more than TIME_PERIOD_FOR_CORE seconds ago)
        self.history = [timestamp for timestamp in self.history if timestamp > now - settings.TIME_PERIOD_FOR_CORE]
        if len(self.history) >= settings.RATE_LIMIT_FOR_CORE:
            # Request limit exceeded
            return False
        # Save the current request to history
        self.history.append(now)
        cache.set(self.key, self.history, timeout=settings.TIME_PERIOD_FOR_CORE)
        return True

    def wait(self):
        """
        Returns the amount of time to wait before the next request is allowed.
        """
        if self.history and len(self.history) >= settings.RATE_LIMIT_FOR_CORE:
            return int(settings.TIME_PERIOD_FOR_CORE - (time.time() - self.history[0]))
        return 0  # No need to wait if the rate limit is not exceeded
