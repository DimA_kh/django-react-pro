from django.urls import path
from core.views import (
    CancelOrderView,
    CreateOrderView,
    FetchOrderView,
    FetchUserOrdersView,
    PaymentCallbackView,
    ProductView,
    ProductsView,
    ProductReservationView,
    ProductSearchView,
)

app_name = "core"

urlpatterns = [
    path("product/", ProductsView.as_view(), name="products"),
    path("product/<int:id>/", ProductView.as_view(), name="one_product"),
    path("product/reservation/", ProductReservationView.as_view(), name="product_reservation"),
    path("product/search/", ProductSearchView.as_view(), name="product_search"),
    path("payment-callback/", PaymentCallbackView.as_view(), name="payment_callback"),
    path("order/", FetchUserOrdersView.as_view(), name="user_orders"),
    path("order/cancel/", CancelOrderView.as_view(), name="cancel_order"),
    path("order/create/", CreateOrderView.as_view(), name="create_order"),
    path("order/<str:id>/", FetchOrderView.as_view(), name="fetch_order"),
]
