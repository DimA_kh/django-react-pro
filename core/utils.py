import hashlib
import uuid
from django.core.cache import cache


def get_user_hash(user_email):
    """
    This function generates a SHA-256 hash for a given user email and caches it.

    The function first checks if the hash for the user email already exists in the cache.
    If it does, the function returns the cached hash.
    If it doesn't, the function generates a new hash, stores it in the cache with a TTL
    of 24 hours, and then returns it.

    Args:
        user_email (str): The user's email address.

    Returns:
        str: The SHA-256 hash of the user's email address.
    """
    user_hash = cache.get(user_email)
    if user_hash is None:
        user_hash = hashlib.sha256(user_email.encode()).hexdigest()
        cache.set(user_email, user_hash, 86400)
    return user_hash


def generate_order_id(user_id):
    """
    This function generates a SHA-256 hash using a user's ID and a UUID.

    Args:
        user_id (int): The user's ID.

    Returns:
        str: The SHA-256 hash.
    """
    raw_string = f"{user_id}{uuid.uuid4()}"
    return hashlib.sha256(raw_string.encode()).hexdigest()
