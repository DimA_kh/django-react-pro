# conftest.py
import pytest


@pytest.fixture(autouse=True)
def mock_throttle_classes(mocker):
    """Fixture to mock throttle_classes to disable throttling."""
    mocker.patch('core.views.CreateOrderView.throttle_classes', new_callable=lambda: [])
    mocker.patch('core.views.CancelOrderView.throttle_classes', new_callable=lambda: [])
