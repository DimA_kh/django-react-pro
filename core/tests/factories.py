from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from factory import (
    Faker,
    SubFactory,
    LazyFunction,
    PostGenerationMethodCall,
    SelfAttribute,
)
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice

from core.models import Order, Product

User = get_user_model()


class UserFactory(DjangoModelFactory):
    email = Faker("email")
    password = PostGenerationMethodCall("set_password", "password")
    is_active = True
    is_superuser = False
    is_staff = False

    class Meta:
        model = User


class StaffUserFactory(UserFactory):
    is_staff = True


class OrderFactory(DjangoModelFactory):
    class Meta:
        model = Order

    buyer = SubFactory(UserFactory)
    buyer_email = SelfAttribute("buyer.email")
    buyer_name = Faker("name")
    phone = "+0123456789000"
    delivery_address = Faker("address")
    delivery_date = LazyFunction(lambda: datetime.now().date() + timedelta(days=1))
    delivery_time = LazyFunction(lambda: datetime.strptime("09:00", "%H:%M").time())
    status = FuzzyChoice(choices=[status[0] for status in Order.ORDER_STATUS])
    products = Faker("text")


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = Product

    title = Faker("sentence")
    price = Faker("random_int", min=10, max=50)
    quantity = Faker("random_int", min=100, max=500)
