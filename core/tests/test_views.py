import pytest
import json

from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory, force_authenticate

from core.models import Order, Product
from core.serializers import OrdersSerializer, ProductSerializer
from core.tests.factories import UserFactory, OrderFactory, ProductFactory
from core.views import CancelOrderView, CreateOrderView, PaymentCallbackView, ProductReservationView

client = APIClient()


@pytest.fixture
def authenticated_user():
    return UserFactory(email="user@example.com", password="password")


class TestCreateOrderView:

    REQUEST_DATA = {
        "buyerName": "Lio Kin",
        "phone": "+123344567890",
        "address": "London",
        "selectedDate": "2023-10-21",
        "selectedTime": "11:00",
        "total": 60,
        "basketItems": [
            {
                "id": 1,
                "title": "Banana",
                "image": "banana.jpg",
                "price": 10,
                "quantity": 70,
                "amount": 5,
            },
            {
                "id": 2,
                "title": "Grapes",
                "image": "grapes.jpg",
                "price": 20,
                "quantity": 100,
                "amount": 2,
            },
        ],
    }

    @pytest.mark.django_db
    def test_create_order_successful(self, mocker, authenticated_user):
        mock_parse_request = mocker.patch("core.views.CreateOrderView.parse_request")
        mock_parse_request.return_value = self.REQUEST_DATA
        mock_cache = mocker.patch("core.views.cache")
        mock_cache.get.return_value = {
            "total": 120,
            "basketItems": self.REQUEST_DATA["basketItems"]
        }
        mock_generate_order_id = mocker.patch("core.views.generate_order_id")
        mock_generate_order_id.return_value = "test_order_id_1"
        mock_initiate_payment = mocker.patch("core.views.initiate_payment")
        mock_initiate_payment.return_value = {
            "signature": "test_signature",
            "data": "test_data"
        }
        mock_get_user_hash = mocker.patch("core.views.get_user_hash")
        mock_get_user_hash.return_value = "user_hash"

        request = APIRequestFactory().post("order/create/", body=self.REQUEST_DATA)
        force_authenticate(request, user=authenticated_user)
        view = CreateOrderView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_201_CREATED
        mock_initiate_payment.assert_called_once()
        assert "order_id" in response.data
        assert Order.objects.get(id=response.data["order_id"]) is not None
        assert "signature" in response.data
        assert "payment_data" in response.data

    @pytest.mark.django_db
    def test_order_overdue_error_response(self, mocker, authenticated_user):
        mocker.patch("core.views.CreateOrderView.parse_request", return_value=self.REQUEST_DATA)
        mocker.patch("core.views.cache.get", return_value=False)
        mocker.patch("core.views.cache.delete")
        mocker.patch("core.views.generate_order_id", return_value="test_order_id_1")
        mocker.patch("core.views.initiate_payment", return_value={"signature": "test_signature", "data": "test_data"})
        mocker.patch("core.views.get_user_hash", return_value="user_hash")

        request = APIRequestFactory().post("order/create/", data={})
        force_authenticate(request, user=authenticated_user)
        view = CreateOrderView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert "The order is already overdue." in response.data["error"]

    @pytest.mark.django_db
    def test_missing_data_error_response(self, mocker, authenticated_user):
        mocker.patch("core.views.CreateOrderView.parse_request", return_value={})
        mocker.patch(
            "core.views.cache.get", return_value={"total": 120, "basketItems": self.REQUEST_DATA["basketItems"]}
        )
        mocker.patch("core.views.cache.delete")
        mocker.patch("core.views.generate_order_id", return_value="test_order_id_1")
        mocker.patch("core.views.initiate_payment", return_value={"signature": "test_signature", "data": "test_data"})
        mocker.patch("core.views.get_user_hash", return_value="user_hash")

        request = APIRequestFactory().post("order/create/", data={})
        force_authenticate(request, user=authenticated_user)
        view = CreateOrderView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert "Not all data received." in response.data["error"]

    @pytest.mark.django_db
    def test_parse_request_valid_data(self, mocker):
        request_mock = mocker.Mock(spec=Request)
        request_mock.body = json.dumps(self.REQUEST_DATA).encode("utf-8")
        create_order_view = CreateOrderView()
        assert create_order_view.parse_request(request_mock)


class TestPaymentCallbackView:
    @pytest.mark.django_db
    def test_payment_callback_successful(self, mocker, authenticated_user):
        order = Order.objects.create(
            id="test_order",
            buyer=authenticated_user,
        )
        mock_process_payment_callback = mocker.patch("core.views.process_payment_callback")
        mock_process_payment_callback.return_value = order.id

        request = APIRequestFactory().post("payment-callback/", data={})
        force_authenticate(request, user=authenticated_user)
        view = PaymentCallbackView.as_view()
        response = view(request)
        order.refresh_from_db()

        assert response.status_code == status.HTTP_200_OK
        assert order.status == Order.PAID

    @pytest.mark.django_db
    def test_payment_callback_invalid(self, mocker, authenticated_user):
        mock_process_payment_callback = mocker.patch("core.views.process_payment_callback")
        mock_process_payment_callback.return_value = None

        request = APIRequestFactory().post("payment-callback/", data={})
        force_authenticate(request, user=authenticated_user)
        view = PaymentCallbackView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_418_IM_A_TEAPOT

    @pytest.mark.django_db
    def test_payment_callback_order_does_not_exist(self, mocker, authenticated_user):
        mock_process_payment_callback = mocker.patch("core.views.process_payment_callback")
        mock_process_payment_callback.return_value = "non_existent_order_id"

        request = APIRequestFactory().post("payment-callback/", data={})
        force_authenticate(request, user=authenticated_user)
        view = PaymentCallbackView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_418_IM_A_TEAPOT


class TestCancelOrderView:
    @pytest.mark.django_db
    def test_cancel_order_successful(self, authenticated_user):
        order = Order.objects.create(
            id="test_order_id",
            buyer=authenticated_user,
        )
        request_data = {"orderId": order.id}
        request = APIRequestFactory().post("order/cancel/", data=request_data)
        force_authenticate(request, user=authenticated_user)
        view = CancelOrderView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_200_OK
        assert response.data["message"] == f"Order {order.id} status updated to CANCELED"
        order.refresh_from_db()
        assert order.status == Order.CANCELED

    @pytest.mark.django_db
    def test_cancel_order_does_not_exist(self, authenticated_user):
        request_data = {"orderId": "non_existent_order_id"}
        request = APIRequestFactory().post("order/cancel/", data=request_data)
        force_authenticate(request, user=authenticated_user)

        view = CancelOrderView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.data["message"] == "Order not found"

    @pytest.mark.django_db
    def test_cancel_order_unexpected_error(self, mocker, authenticated_user):
        order = Order.objects.create(
            id="test_order_id",
            buyer=authenticated_user,
        )

        request_data = {"orderId": order.id}
        request = APIRequestFactory().post("order/cancel/", data=request_data)
        force_authenticate(request, user=authenticated_user)
        mock_order_save = mocker.patch.object(Order, "save", side_effect=Exception("Unexpected error"))

        view = CancelOrderView.as_view()
        response = view(request)

        assert response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR
        assert response.data["message"] == "Unexpected error"
        mock_order_save.assert_called_once()


class TestProductReservationView:
    @pytest.mark.django_db
    def test_check_stock(self):
        """
        This test verifies that the product is in stock.
        """
        self.products = []
        for item in ("Banana", "Grapes", "Apple", "Orange"):
            self.products.append(ProductFactory(title=item))
        view = ProductReservationView()
        basket = {
            self.products[0].id: 7,
            self.products[1].id: 2,
            self.products[3].id: 5,
        }
        basket_cache = {}
        is_in_stock = view.check_stock(basket, basket_cache)

        assert is_in_stock is True

    @pytest.mark.django_db
    def test_check_no_stock(self):
        """
        This test confirms that the product is out of stock.
        """
        self.products = []
        for item in ("Banana", "Grapes", "Apple", "Orange"):
            self.products.append(ProductFactory(title=item))
        view = ProductReservationView()
        basket = {
            self.products[0].id: 900000,
            self.products[1].id: 2000,
            self.products[3].id: 2,
        }
        basket_cache = {}
        is_in_stock = view.check_stock(basket, basket_cache)

        assert is_in_stock is False

    @pytest.mark.django_db
    def test_get_cache_basket_products(self, mocker):
        """
        Test that the `get_cache_basket_products` method correctly retrieves cached basket products.
        """
        CACHE_DATA = {
            "basketItems": [
                {"id": 1, "amount": 2},
                {"id": 2, "amount": 3},
                {"id": 3, "amount": 5},
            ]
        }
        full_cache = mocker.patch("core.views.cache.get")
        full_cache.return_value = {"basketItems": CACHE_DATA["basketItems"]}
        view = ProductReservationView()
        cache_basket_products = view.get_cache_basket_products("test_cache_id", "test_cache_ttl_id")
        expected_basket_products = {item["id"]: item["amount"] for item in CACHE_DATA["basketItems"]}

        assert cache_basket_products == expected_basket_products

    @pytest.mark.django_db
    def test_get_cache_basket_products_if_cache_empty(self, mocker):
        """
        Test that the `get_cache_basket_products` method correctly handles an empty cache.
        """
        empty_cache = mocker.patch("core.views.cache.get")
        empty_cache.return_value = False
        view = ProductReservationView()
        cache_basket_products = view.get_cache_basket_products("test_cache_id", "test_cache_ttl_id")

        assert cache_basket_products == {}

    @pytest.mark.django_db
    @pytest.mark.parametrize("input_data, expected_result", [
        (
            {
                "new_basket_products": {1: 5, 2: 10, 3: 15},
                "cache_basket_products": {1: 5, 2: 10, 3: 15}
            },
            {
                "count_products_for_update": 0,
                "products_for_update": []
            }
        ),
        (
            {
                "new_basket_products": {1: 15, 2: 20, 3: 35},
                "cache_basket_products": {1: 5, 2: 10, 3: 15}
            },
            {
                "count_products_for_update": 3,
                "products_for_update": [
                    Product(id=1, quantity=0),
                    Product(id=2, quantity=10),
                    Product(id=3, quantity=10)
                ]
            }
        ),
        (
            {
                "new_basket_products": {1: 5, 2: 5, 3: 5},
                "cache_basket_products": {1: 15, 2: 35, 3: 45}
            },
            {
                "count_products_for_update": 3,
                "products_for_update": [
                    Product(id=1, quantity=20),
                    Product(id=2, quantity=50),
                    Product(id=3, quantity=70)
                ]
            }
        ),
        (
            {
                "new_basket_products": {1: 5, 2: 10, 3: 15, 4: 20},
                "cache_basket_products": {4: 50, 5: 60, 6: 70}
            },
            {
                "count_products_for_update": 6,
                "products_for_update": [
                    Product(id=4, quantity=70),
                    Product(id=5, quantity=110),
                    Product(id=6, quantity=130),
                    Product(id=1, quantity=5),
                    Product(id=2, quantity=10),
                    Product(id=3, quantity=15)
                ]
            }
        ),
    ])
    def test_compare_basket(self, input_data, expected_result):
        all_products = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
        product_reservatio_class_instance = ProductReservationView(all_products=all_products)
        result = product_reservatio_class_instance.compare_basket(
            input_data["new_basket_products"],
            input_data["cache_basket_products"]
        )

        assert len(result) == expected_result["count_products_for_update"]
        for actual_product, expected_product in zip(result, expected_result["products_for_update"]):
            assert isinstance(actual_product, Product)
            assert actual_product.id == expected_product.id
            assert actual_product.quantity == expected_product.quantity

    @pytest.mark.django_db
    @pytest.mark.parametrize("return_value", [
        [], [Product(id=3, quantity=15)]
    ])
    def test_update_products(self, mocker, return_value):
        mock_compare_basket = mocker.patch("core.views.ProductReservationView.compare_basket")
        mock_compare_basket.return_value = return_value
        mock_bulk_update = mocker.patch("core.views.Product.objects.bulk_update")
        ProductReservationView().update_products(
            {1: 10, 2: 20, 3: 30},
            {4: 40, 5: 50, 6: 60}
        )

        mock_compare_basket.assert_called_once_with({1: 10, 2: 20, 3: 30}, {4: 40, 5: 50, 6: 60})
        if return_value:
            mock_bulk_update.assert_called_once()
        else:
            mock_bulk_update.assert_not_called()


class ProductViewTest(APITestCase):
    client = APIClient()

    def setUp(self):
        Product.objects.create(id=1, title=1)

    def test_get_product(self):
        response = self.client.get(reverse("core:one_product", kwargs={"id": 1}))
        expected = Product.objects.get(id=1)
        serialized = ProductSerializer(expected)

        self.assertEqual(response.data, {"product": serialized.data})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_product_not_found(self):
        response = self.client.get(reverse("core:one_product", kwargs={"id": 2}))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ProductsViewTest(APITestCase):
    def test_get_products(self):
        Product.objects.create(title=1)
        Product.objects.create(title=2)
        response = self.client.get(reverse("core:products"))
        expected = Product.objects.all()
        serialized = ProductSerializer(expected, many=True)

        self.assertEqual(response.data, {"products": serialized.data})
        self.assertEqual(response.status_code, status.HTTP_200_OK)


@pytest.mark.django_db(transaction=True)
class FetchUserOrdersViewTest(APITestCase):
    def test_get_orders(self):
        test_user = UserFactory(id=1, email="user@example.com", password="password")
        for index in range(3):
            OrderFactory(id=index, buyer=test_user)
        self.client.force_authenticate(user=test_user)
        response = self.client.get(reverse("core:user_orders"))
        expected_result = Order.objects.filter(buyer=test_user)
        serialized = OrdersSerializer(expected_result, many=True)

        self.assertEqual(response.data, {"orders": serialized.data})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
