import logging
from datetime import datetime, timedelta
from django.conf import settings
from liqpay import LiqPay


def initiate_payment(order):
    """
    To initiate a payment and communicate with LiqPay.

    Returns:
        Response: A Response object with the payment URL if the payment initiation is successful,
        or an error response if the request data is invalid.
    """

    if not order:
        logging.warning("Order not exist")
        raise Exception("Order not exist")

    liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)

    # Set a 9-minute payment deadline
    payment_deadline = datetime.now() + timedelta(minutes=9)
    expired_date = payment_deadline.strftime("%Y-%m-%d %H:%M:%S")

    # Prepare payment data for LiqPay
    payment_data = {
        "action"         : "hold",                                                  # or "pay"
        "public_key"     : settings.LIQPAY_PUBLIC_KEY,
        "version"        : "3",
        "amount"         : order.total,
        "currency"       : "USD",
        "description"    : f"Payment for {order.buyer_name}'s grocires.",
        # "info"           : order,
        "order_id"       : order.id,
        "expired_date"   : expired_date,
        "server_url"     : f"https://{settings.NGROK_URL}/api/payment-callback/",
        "sandbox"        : 1,
    }

    # In cnb_signature (liqpay lib) need remove last self._private_key
    params = {
        'data': liqpay.cnb_data(payment_data),
        'signature': liqpay.cnb_signature(payment_data),
    }

    return params
    # try:
    #     response = requests.post(url=settings.LIQPAY_CHECKOUT_URL, data=params)
    #     if response.status_code == 200:
    #         logging.info(f"LIQPAY: time - {timezone.now()} | order - {order_in_cache['order_id']}")
    #         print(response.url)
    #         return response.url
    #     else:
    #         logging.warning(f"time {timezone.now()}| incorrect status code form "
    #                         f"response - {response.status_code}, must be 200, "
    #                         f"data- {payment_data}, params - {params}")
    #         raise Exception("Error LiqPay")
    # except:
    #     logging.exception(f"error getting response from liqpay, data- {payment_data}, params - {params}")
    #     raise Exception("Error LiqPay")


def process_payment_callback(request):
    """
    Handle a request from LiqPay with payment callback data.
    """
    data = request.POST.get('data')
    signature = request.POST.get('signature')
    liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)

    # Verify the signature
    sign = liqpay.str_to_sign(settings.LIQPAY_PRIVATE_KEY + data + settings.LIQPAY_PRIVATE_KEY)
    if sign != signature:
        return False

    # Decode the data from LiqPay
    decoded_data = liqpay.decode_data_from_str(data)
    transaction_state = decoded_data.get('status').lower()

    if transaction_state not in ('success', 'sandbox', 'hold_wait'):
        return False

    return decoded_data.get('order_id')
