import json
import logging

from django.core.cache import cache
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from core.meilisearch_client import MeiliSearchClient
from core.models import Order, Product
from core.payment.liqpay_logic import initiate_payment, process_payment_callback
from core.serializers import OrdersSerializer, ProductSerializer
from core.throttles import IPBasedThrottleCore
from core.utils import generate_order_id, get_user_hash

log = logging.getLogger(__name__)
ORDER_LOG = logging.getLogger('order_logger')


class ProductsView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        log.info("Fetching all products.")
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        log.info(f"Fetched {len(products)} products.")
        return Response({"products": serializer.data})


class ProductView(APIView):
    def get(self, request, id):
        log.info(f"Fetching product with ID: {id}")
        try:
            product = Product.objects.get(id=id)
            serializer = ProductSerializer(product)
            log.info(f"Product {id} fetched successfully.")
            return Response({"product": serializer.data})
        except Product.DoesNotExist:
            log.warning(f"Product with ID {id} not found.")
            return Response(status=status.HTTP_404_NOT_FOUND)


class FetchUserOrdersView(APIView):
    def get(self, request):
        log.info(f"Fetching orders for user: {request.user}.")
        try:
            orders = Order.objects.filter(buyer=request.user)
            serializer = OrdersSerializer(orders, many=True)
            ORDER_LOG.info(f"Fetched {len(orders)} orders for user {request.user}.")
            return Response({"orders": serializer.data})
        except ObjectDoesNotExist as ex:
            ORDER_LOG.warning(f"No orders found for user {request.user}: {ex}.")
            return Response({"error": "No orders found."}, status=404)
        except ValidationError as ex:
            ORDER_LOG.error(f"Validation error for user {request.user}: {ex}.", exc_info=True)
            return Response({"error": "Invalid data."}, status=400)
        except Exception as ex:
            ORDER_LOG.error(f"Error fetching orders for user {request.user}: {ex}.", exc_info=True)
            raise AuthenticationFailed('Unauthorized')


class FetchOrderView(APIView):
    def get(self, request, id):
        ORDER_LOG.info(f"Fetching order with ID: {id}.")
        try:
            order = Order.objects.get(id=id)
            order_data = {
                "id": order.id,
                "buyer_name": order.buyer_name,
                "status": order.status,
                "delivery_address": order.delivery_address,
                "delivery_date": order.delivery_date,
                "delivery_time": order.delivery_time,
                "phone": order.phone,
                "order_detail_items": order.products,
            }
            ORDER_LOG.info(f"Order {id} fetched successfull.")
            return Response({"data": order_data}, status=status.HTTP_200_OK)
        except Order.DoesNotExist:
            ORDER_LOG.warning(f"Order with ID {id} not found.")
            return Response({"error": "Order not found"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as ex:
            ORDER_LOG.error(f"Error fetching order {id}: {ex}.", exc_info=True)
            return Response({"error": str(ex)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProductReservationView(APIView):
    throttle_classes = [IPBasedThrottleCore]

    def post(self, request, *args, **kwargs):
        log.info("Starting product reservation process.")
        data = json.loads(request.body.decode("utf-8"))
        if not data:
            log.warning("Product reservation failed: incomplete data.")
            return Response({"error": "Not all data received."}, status=status.HTTP_400_BAD_REQUEST)

        raw_cache_id = get_user_hash(request.user.email)
        cache_id = f"user_{raw_cache_id}"
        cache_ttl_id = f"user_{raw_cache_id}:ttl"

        cache_basket_products = self.get_cache_basket_products(cache_id, cache_ttl_id)
        basket_products = {item["id"]: item["amount"] for item in data.get("basketItems")}

        if not self.check_stock(basket_products, cache_basket_products):
            log.warning("Product reservation failed: insufficient stock.")
            return Response(
                {"message": "Not enough stock for some products."},
                status=status.HTTP_400_BAD_REQUEST
            )

        self.update_products(basket_products, cache_basket_products)

        # Save the order in cache (lifetime 1 hour)
        cache.set(cache_id, data, 3600)
        # Create a control record of the lifetime of this order (lifetime 10 minutes)
        cache.set(cache_ttl_id, 'exists', 600)

        log.info(f"Products reserved successfully for user: {request.user}.")
        return Response({"products_reserved": data}, status=status.HTTP_200_OK)

    def get_cache_basket_products(self, cache_id, cache_ttl_id):
        # Get the cached basket products if they exist
        cache_basket_products = {}
        if cache.get(cache_ttl_id) and cache.get(cache_id):
            cache_basket_products = {item["id"]: item["amount"] for item in cache.get(cache_id)["basketItems"]}
        return cache_basket_products

    def check_stock(self, basket_products, cache_basket_products):
        # Check if there is enough stock for all products
        all_id = set(basket_products.keys()) | set(cache_basket_products.keys())
        all_items = Product.objects.filter(id__in=all_id)
        self.all_products = {product.id: product.quantity for product in all_items}
        for item_id, amount in basket_products.items():
            if amount > self.all_products.get(item_id):
                return False
        return True

    def update_products(self, basket_products, cache_basket_products):
        # Update the quantities of the products
        if products := self.compare_basket(basket_products, cache_basket_products):
            Product.objects.bulk_update(products, ["quantity"])

    def compare_basket(self, new_basket_products, cache_basket_products):
        products_for_update = []
        for id in cache_basket_products:
            # Quantity of goods in cache and quantity in a new cart
            diff_amount = cache_basket_products[id] - new_basket_products.pop(id, 0)
            if diff_amount == 0:
                continue
            else:
                # product must be returned to the warehouse (if diff_amount > 0)
                # or take from the warehouse (if diff_amount < 0)
                new_quantity = self.all_products[id] + diff_amount
            products_for_update.append(
                Product(
                    id=id,
                    quantity=new_quantity
                )
            )

        for id in new_basket_products:
            products_for_update.append(
                Product(
                    id=id,
                    quantity=self.all_products[id] - new_basket_products[id])
            )
        return products_for_update


class CancelOrderView(APIView):
    @transaction.non_atomic_requests
    def post(self, request, *args, **kwargs):
        order_id = request.data.get("orderId")
        ORDER_LOG.info(f"Attempting to cancel order {order_id}.")
        try:
            order = Order.objects.get(id=order_id)
            order.status = Order.CANCELED
            order.save()
            ORDER_LOG.info(f"Order {order_id} canceled successfully.")
            return Response({"message": f"Order {order_id} status updated to CANCELED"}, status=status.HTTP_200_OK)
        except Order.DoesNotExist:
            ORDER_LOG.warning(f"Order {order_id} not found.", exc_info=True)
            return Response({"message": "Order not found"}, status=status.HTTP_404_NOT_FOUND)
        except Exception as ex:
            ORDER_LOG.error(f"Error canceling order {order_id}: {ex}.", exc_info=True)
            return Response({"message": str(ex)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CreateOrderView(APIView):
    throttle_classes = [IPBasedThrottleCore]

    def post(self, request, *args, **kwargs):
        """
        Create a new order with the provided basket items.

        Returns:
            Response: HTTP response with order ID if successful, or an error response.

        Raises:
            HTTP 400 Bad Request: If there is not enough stock for any product.
            HTTP 403 Forbidden: Number of requests per minute exceeded.
            HTTP 500 Internal Server Error: If an internal server error occurs.
        """
        request_data = self.parse_request(request)
        if not request_data:
            ORDER_LOG.warning("Order creation failed: incomplete data")
            return Response({"error": "Not all data received."}, status=status.HTTP_400_BAD_REQUEST)

        raw_cache_id = get_user_hash(request.user.email)
        cache_id = f"user_{raw_cache_id}"
        cache_ttl_id = f"user_{raw_cache_id}:ttl"

        if not cache.get(cache_id) and not cache.get(cache_ttl_id):
            ORDER_LOG.warning("Order creation failed: order is overdue")
            return Response({"error": "The order is already overdue."}, status=status.HTTP_400_BAD_REQUEST)

        try:
            order = Order.objects.create(
                id=generate_order_id(request.user.id),
                buyer=request.user,
                buyer_email=request.user.email,
                buyer_name=request_data["buyerName"],
                delivery_address=request_data["address"],
                phone=request_data["phone"],
                delivery_date=request_data["selectedDate"],
                delivery_time=request_data["selectedTime"],
                total=cache.get(cache_id)["total"],
                products=cache.get(cache_id)["basketItems"],
            )
            params = initiate_payment(order)
            cache.delete(cache_id)
            ORDER_LOG.info(f"Order {order.id} created successfully.")
            return Response({
                "order_id": order.id,
                "signature": params["signature"],
                "payment_data": params["data"]
            }, status=status.HTTP_201_CREATED)
        except Exception as ex:
            ORDER_LOG.error(f"Order creation failed: {ex}.", exc_info=True)
            return Response({"error": str(ex)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def parse_request(self, request):
        """
        Parse the request and check if all required data is received.
        """
        request_data = json.loads(request.body.decode("utf-8"))
        required_fields = ["buyerName", "phone", "address", "selectedDate", "selectedTime"]
        if all(field in request_data for field in required_fields):
            return request_data


@method_decorator(csrf_exempt, name="dispatch")
class PaymentCallbackView(APIView):
    def post(self, request, *args, **kwargs):
        """
        Handle a POST request from LiqPay with payment callback data.
        POST `/api/payment-callback/`

        Returns:
            A Response object with a success code 200 if the payment callback
            is valid and the order is updated successfully, or a 418 response
            if the signature or order is invalid.
        """
        order_id = process_payment_callback(request)
        ORDER_LOG.info(f"Processing payment callback. Order ID: {order_id}.")
        if not order_id:
            # Available HTTP codes: 200, 301, 302, 303, 307, 418.
            # For all other response codes, LiqPay will send the callback again (infinite number of times).
            ORDER_LOG.warning(f"Invalid payment callback: signature or order ID {order_id} issue.")
            return HttpResponse(status=status.HTTP_418_IM_A_TEAPOT)
        try:
            order = Order.objects.get(id=order_id)
        except Order.DoesNotExist:
            ORDER_LOG.warning(f"Payment callback failed: order {order_id} not found.")
            return HttpResponse(status=status.HTTP_418_IM_A_TEAPOT)
        order.status = Order.PAID
        order.save()
        ORDER_LOG.info(f"Order {order_id} marked as PAID.")

        return HttpResponse(status=status.HTTP_200_OK)


class ProductSearchView(APIView):
    def get(self, request):
        """
        Handles GET requests to search for products.

        Args:
            request (HttpRequest): The HTTP request object.

        Returns:
            Response: A response containing search results or an error message.
        """
        query = request.GET.get("find", "")
        if not query:
            return Response({"error": "Query parameter is required"}, status=status.HTTP_400_BAD_REQUEST)

        client = MeiliSearchClient()
        try:
            # Perform search using the MeiliSearch client
            search_results = client.search("products", query)
            return Response(search_results, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response({"error": str(ex)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
