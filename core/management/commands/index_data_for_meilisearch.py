"""
Indexes data of Product in Meilisearch.
"""

import logging

from django.conf import settings
from django.core.management.base import BaseCommand
from core.models import Product
import meilisearch

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Indexes data of Product in Meilisearch"

    def handle(self, *args, **kwargs):
        client = meilisearch.Client(settings.MEILI_HOST, settings.MEILI_MASTER_KEY)
        products = Product.objects.all().values('id', 'title', 'image', 'price', 'quantity', 'description')
        client.index('products').add_documents(list(products))
        self.stdout.write(self.style.SUCCESS("[Meilisearch] Products indexed successfully!"))
