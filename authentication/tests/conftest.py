import pytest


@pytest.fixture(autouse=True)
def mock_throttle_classes(mocker):
    """Fixture to mock throttle_classes to disable throttling."""
    mocker.patch('authentication.views.PasswordResetConfirmView.throttle_classes', new_callable=lambda: [])
    mocker.patch('authentication.views.PasswordResetView.throttle_classes', new_callable=lambda: [])
