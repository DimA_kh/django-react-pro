import pytest
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from rest_framework.test import APIRequestFactory
from rest_framework import status

from authentication.models import PasswordResetRequest
from authentication.views import (
    PasswordResetView,
    PasswordResetConfirmView,
)
from core.tests.factories import UserFactory


@pytest.fixture
def authenticated_user():
    return UserFactory(email="user@example.com", password="password")


@pytest.mark.django_db
def test_password_reset_successful(mocker, authenticated_user):
    factory = APIRequestFactory()
    request_data = {'email': authenticated_user.email}
    request = factory.post("/password-reset/", data=request_data)
    view = PasswordResetView.as_view()

    mock_send_email = mocker.patch("authentication.tasks.send_password_reset_email.send")
    mock_send_email.return_value = None

    response = view(request)

    assert response.status_code == status.HTTP_200_OK
    mock_send_email.assert_called_once_with(authenticated_user.email)


@pytest.mark.django_db
def test_password_reset_confirm_successful(mocker, authenticated_user):
    reset_request = PasswordResetRequest.objects.create(user=authenticated_user)
    token = default_token_generator.make_token(authenticated_user)
    uid = urlsafe_base64_encode(str(authenticated_user.pk).encode('utf-8'))

    factory = APIRequestFactory()
    request_data = {
        'uid': uid,
        'token': token,
        'new_password': "newpassword",
        'confirm_password': "newpassword",
    }
    request = factory.put("/password-reset-confirm/", data=request_data)
    view = PasswordResetConfirmView.as_view()

    mock_is_valid = mocker.patch.object(PasswordResetRequest, 'is_valid', return_value=True)

    response = view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data['message'] == "Password has been reset"
    authenticated_user.refresh_from_db()
    assert authenticated_user.check_password('newpassword')
    reset_request.refresh_from_db()
    assert reset_request.is_used
    mock_is_valid.assert_called_once()
