import logging

from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.tokens import RefreshToken, UntypedToken

logger = logging.getLogger(__name__)


class JWTAdminMiddleware:
    """
    Middleware for authenticating users to Django admin using JWT tokens stored in cookies.

    This middleware intercepts incoming HTTP requests and checks for the presence of a JWT token
    in the cookies. If a valid token is found, it authenticates the user and sets the `request.user`
    to the authenticated user, allowing access to the Django admin without requiring a separate login.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.user = self.get_user_from_token(request)
        response = self.get_response(request)
        response.set_cookie(
            'shop-access',
            self.access_token,
            max_age=60,
        )
        return response

    def get_user_from_token(self, request):
        jwt_authenticator = JWTAuthentication()
        try:
            self.access_token = request.COOKIES.get('shop-access')
            UntypedToken(self.access_token)
            validated_token = jwt_authenticator.get_validated_token(self.access_token)
            return jwt_authenticator.get_user(validated_token)
        except (InvalidToken, TokenError):
            try:
                refresh = RefreshToken(request.COOKIES.get('shop-refresh'))
                self.access_token = str(refresh.access_token)
                validated_token = jwt_authenticator.get_validated_token(self.access_token)
                return jwt_authenticator.get_user(validated_token)
            except (InvalidToken, TokenError):
                return AnonymousUser()


class AdminLogoutMiddleware:
    """
    Middleware that deletes specific cookies when user logout.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if request.path == '/admin/logout/':
            response.delete_cookie('shop-refresh')
            response.delete_cookie('shop-access')
            response.delete_cookie('shop-user')
        if request.path == '/admin/login/':
            return HttpResponseRedirect(settings.LOGOUT_REDIRECT_URL)
        return response
