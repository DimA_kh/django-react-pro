import logging
from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.timezone import now
from django.template.loader import render_to_string
from dramatiq import actor

log = logging.getLogger(__name__)


@actor
def send_password_reset_email(email):
    """
    Asynchronous task to send a password reset email to the user.
    """
    # Avoid circular import issues
    from authentication.models import User, PasswordResetRequest

    try:
        user = User.objects.get(email=email)
    except User.DoesNotExist:
        log.error(f"User with ID: {email} does not exist.")
        return

    token = default_token_generator.make_token(user)
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    reset_url = f"http://{settings.FRONTEND_HOST}/password-reset/{uid}/{token}/"

    PasswordResetRequest.objects.update_or_create(
        user=user,
        defaults={
            'created_at': now(),
            'is_used': False
        }
    )

    context = {
        'user': user,
        'reset_url': reset_url,
    }

    text_message = render_to_string('email/password_reset/body.txt', context)
    html_message = render_to_string('email/password_reset/body.html', context)

    email_subject = 'Password reset for "Shop number one"'
    email_from = "from-shop@example.com"
    email_to = [user.email]

    email_obj = EmailMultiAlternatives(
        subject=email_subject,
        body=text_message,
        from_email=email_from,
        to=email_to,
    )
    email_obj.attach_alternative(html_message, "text/html")
    email_obj.send()

    log.info(f"Password reset email sent to {email}.")
