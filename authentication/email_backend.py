from django.core.mail.backends.console import EmailBackend


class PlainTextConsoleEmailBackend(EmailBackend):
    def write_message(self, message):
        print("-----------------------------------------------------------")
        print("Subject:", message.subject)
        print("To:", ", ".join(message.to))
        print("From:", message.from_email)
        print("Body:\n", message.body)
        print("-----------------------------------------------------------")
