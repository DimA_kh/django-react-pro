from datetime import timedelta

from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)
from django.contrib.auth.signals import user_logged_out
from django.db import models
from django.dispatch import receiver
from django.utils.timezone import now
from rest_framework_simplejwt.tokens import RefreshToken


class UserManager(BaseUserManager):
    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self._create_user(email, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
    """
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=30, blank=True)
    phone = models.CharField(max_length=20, blank=True)
    address = models.CharField(max_length=240, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_name(self):
        return self.name

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self


class PasswordResetRequest(models.Model):
    """
    Model for managing user password reset requests.

    This model stores information about password reset requests made by users.
    Each request is associated with a user and contains the timestamp when the
    request was created, and a flag indicating whether the request has been used.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='password_reset_request')
    created_at = models.DateTimeField(auto_now_add=True)
    is_used = models.BooleanField(default=False)

    def is_valid(self):
        """
        Check if the password reset request is valid.
        """
        # Link is valid for 1 hour
        expiration_time = self.created_at + timedelta(hours=1)
        return not self.is_used and now() < expiration_time


@receiver(user_logged_out)
def invalidate_tokens_on_logout(sender, request, user, **kwargs):
    refresh_token = request.COOKIES.get('shop-refresh')
    refresh_token = RefreshToken(refresh_token)
    refresh_token.blacklist()
