from django.contrib import admin
from authentication.models import User, PasswordResetRequest


@admin.register(User)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('email', 'is_active', 'is_staff', 'date_joined')
    readonly_fields = ('password',)


@admin.register(PasswordResetRequest)
class PasswordResetRequestAdmin(admin.ModelAdmin):
    list_display = ('user', 'is_used', 'created_at')
    readonly_fields = ('user', 'is_used', 'created_at')
