import logging

from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator

from rest_framework import status
from rest_framework.generics import UpdateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from authentication.models import User, PasswordResetRequest
from authentication.serializers import (
    UserSerializer,
    UserRegistrSerializer,
    PasswordResetSerializer,
)
from authentication.tasks import send_password_reset_email
from authentication.throttles import IPBasedThrottleAuthn

log = logging.getLogger(__name__)


class RegisterAPIView(APIView):
    """
    API view to handle user registration.
    """
    serializer_class = UserRegistrSerializer

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests for registering a new user.

        Return:
            If the email already exists, returns a 409 Conflict response.
            If the data is valid, creates a new user and returns a 201 Created response.
            Otherwise, returns a 400 Bad Request response with error details.
        """
        email = request.data.get('email', '')
        if User.objects.filter(email=email).exists():
            data = {'message': "User with this email already exists."}
            return Response(data, status=status.HTTP_409_CONFLICT)

        serializer = UserRegistrSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = {'message': "User created successfully."}
            return Response(data, status=status.HTTP_201_CREATED)
        else:
            data = serializer.errors
            data['message'] = "There is something wrong with your request."
            return Response(data, status=status.HTTP_400_BAD_REQUEST)


class UserRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    """
    API view to retrieve or update the authenticated user's information.
    """
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        """
        Retrieves the authenticated user's information.
        """
        serializer = self.serializer_class(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        """
        Updates the authenticated user's information with the provided data.
        """
        serializer_data = request.data.get('user', {})
        serializer = UserSerializer(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


class AllUserAPIView(APIView):
    """
    API view to list all registered users.
    """
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        serializer = self.serializer_class(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PasswordResetView(APIView):
    """
    API view to initiate the password reset process.
    """
    throttle_classes = [IPBasedThrottleAuthn]

    def post(self, request, *args, **kwargs):
        """
        Sends a password reset email asynchronously using a Dramatiq task.
        """
        email = request.data.get('email')
        if not email:
            return Response({'error': "Email is required"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(email=email)
            # call the Dramatiq task
            send_password_reset_email.send(user.email)
            log.info(f"Password reset email task triggered for {email}.")
        except User.DoesNotExist:
            log.error(f"Attempt to reset password for non-existent email {email}.")
        # Always display a confirmation message stating that the email has been successfully sent,
        # regardless of whether the email address exists in the system, to maintain the
        # confidentiality of the user's presence in the system.
        return Response(status=status.HTTP_200_OK)


class PasswordResetConfirmView(UpdateAPIView):
    """
    API view to handle password reset confirmation.
    """
    permission_classes = [AllowAny]
    serializer_class = PasswordResetSerializer
    throttle_classes = [IPBasedThrottleAuthn]

    def update(self, request, *args, **kwargs):
        """
        Validate the reset token and update the user's password.
        """
        uidb64 = request.data.get('uid')
        token = request.data.get('token')

        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
            reset_request = PasswordResetRequest.objects.get(user=user, is_used=False)
            if not reset_request.is_valid():
                return Response({'message': 'Invalid or expired token'}, status=status.HTTP_400_BAD_REQUEST)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist, PasswordResetRequest.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                user.set_password(serializer.validated_data.get('new_password'))
                user.save()
                # Mark the token as used
                reset_request.is_used = True
                reset_request.save()
                return Response({'message': 'Password has been reset'}, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)
