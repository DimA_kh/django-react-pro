from django.urls import path
from rest_framework_simplejwt.views import (
    TokenRefreshView,
    TokenVerifyView,
    TokenBlacklistView,
    TokenObtainPairView
)
from authentication.views import (
    RegisterAPIView,
    UserRetrieveUpdateAPIView,
    AllUserAPIView,
    PasswordResetView,
    PasswordResetConfirmView,
)
from authentication.serializers import UserTokenObtainSerializer

app_name = "auth"

urlpatterns = [
    path("login/", TokenObtainPairView.as_view(serializer_class=UserTokenObtainSerializer), name='token_obtain_pair'),
    path("logout/", TokenBlacklistView.as_view(), name='token_blacklist'),
    path("refresh/", TokenRefreshView.as_view(), name='token_refresh'),
    path("verify/", TokenVerifyView.as_view(), name='token_verify'),
    path("register/", RegisterAPIView.as_view(), name='register'),
    path("update/", UserRetrieveUpdateAPIView.as_view()),
    path("users/", AllUserAPIView.as_view()),
    path("password-reset/", PasswordResetView.as_view(), name='password_reset'),
    path("password-reset-confirm/", PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
]
