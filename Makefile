.DEFAULT_GOAL := help

FRONTEND_IMAGE = shop-frontend-image:latest
BACKEND_IMAGE = shop-backend-image:latest
COMPOSE_FILE = docker-compose.yml
DOCKER_COMPOSE = docker-compose -f $(COMPOSE_FILE)

.PHONY: build-frontend build-backend build
.PHONY: start stop restart
.PHONY: migrate createuser shell

# Help message
help:
	@echo "Available targets:"
	@echo "  build-frontend       Build the frontend Docker image for development without cache"
	@echo "  build-backend        Build the backend Docker image for development without cache"
	@echo "  build-dev            Build Docker images for development"
	@echo "  build-prod           Build Docker images for production"
	@echo "  start-dev            Start containers in development mode with hot-reload"
	@echo "  start-prod           Start containers in production mode"
	@echo "  stop                 Stop containers"
	@echo "  restart              Restart containers"
	@echo "  migrate              Execute database migrations"
	@echo "  createuser           Create superuser"

set-executable:
	chmod +x install-frontend-dependence.sh

# Build Docker images
build-frontend:
	docker build --target development -t $(FRONTEND_IMAGE) ./frontend --no-cache

build-backend:
	docker build --target development -t $(BACKEND_IMAGE) . --no-cache

build-dev:
	@echo "Building Docker images..."
	TARGET_STAGE=development $(DOCKER_COMPOSE) build --no-cache

build-prod:
	@echo "Building Docker images..."
	TARGET_STAGE=production $(DOCKER_COMPOSE) build --no-cache

# Start, stop and restart containers
start-dev:
	TARGET_STAGE=development $(DOCKER_COMPOSE) up -d

start-prod:
	TARGET_STAGE=production $(DOCKER_COMPOSE) up -d

stop:
	$(DOCKER_COMPOSE) stop

restart: stop start

# Execute database migrations and create superuser
migrate:
	@echo "Applying database migrations..."
	$(DOCKER_COMPOSE) exec backend python manage.py migrate

create-superuser:
	@echo "Creating superuser account..."
	$(DOCKER_COMPOSE) exec backend python create-superuser.py
	@echo "Superuser admin@mail.com successfully created."

shell.%:
	$(DOCKER_COMPOSE) exec $* sh

# Provision project
provision: set-executable
	@echo "Provisioning project..."
	./install-frontend-dependence.sh
	$(MAKE) build-dev
	$(MAKE) start-dev
	$(MAKE) migrate
	$(MAKE) create-superuser

test:
	$(DOCKER_COMPOSE) exec backend pytest -rA

lint:
	$(DOCKER_COMPOSE) exec backend flake8 --jobs=1 --verbose
