from django.conf import settings


def frontend_host(request):
    return {
        'FRONTEND_HOST': settings.FRONTEND_HOST
    }
