"""These views use only HTMX."""

from django.views.generic import DetailView, ListView
from core.models import Order


class DashboardView(ListView):
    template_name = 'seller_dashboard/dashboard.html'
    context_object_name = 'orders'
    paginate_by = 20

    def get_queryset(self):
        self.status = self.request.GET.get('status', None)
        sort = self.request.GET.get('sort', 'created_at')
        orders = Order.objects.all()
        if self.status:
            orders = orders.filter(status=self.status)
        return orders.order_by(sort)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.headers.get('HX-Request'):
            self.template_name = 'seller_dashboard/orders_list_data.html'
        return context


class OrderDetailForAdminView(DetailView):
    model = Order
    template_name = 'seller_dashboard/order_detail.html'
    context_object_name = 'order'
