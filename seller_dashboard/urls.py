from functools import wraps

from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import path

from seller_dashboard.views import (
    DashboardView,
    OrderDetailForAdminView
)


def staff_required(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        current_user = request.user
        if current_user.is_authenticated and (current_user.is_staff or current_user.is_superuser):
            return func(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(settings.LOGOUT_REDIRECT_URL)
    return wrapper


app_name = "seller_dashboard"

urlpatterns = [
    path("", staff_required(DashboardView.as_view()), name="dashboard"),
    path("order/<str:pk>/", staff_required(OrderDetailForAdminView.as_view()), name="order_detail"),
]
