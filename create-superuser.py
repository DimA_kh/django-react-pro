import os
import django
from django.db import IntegrityError

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shop.settings")
django.setup()

from authentication.models import User


def main():
    try:
        User.objects.create_superuser(
            email='admin@mail.com',
            password='admin',
            name='admin',
        )
    except IntegrityError:
        pass


if __name__ == "__main__":
    main()
