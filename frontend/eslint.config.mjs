import eslintPluginReact from 'eslint-plugin-react';
import eslintPluginJSX from 'eslint-plugin-jsx-a11y';
import babelEslintParser from '@babel/eslint-parser';

export default [
  {
    files: ['**/*.js', '**/*.jsx', '**/*.test.js', '**/*.test.jsx'],
    languageOptions: {
      ecmaVersion: 2021,
      sourceType: 'module',
      parser: babelEslintParser,
      parserOptions: {
        requireConfigFile: false,
        ecmaFeatures: {
          jsx: true,
        }
      },
      globals: {
        browser: true,
        node: true,
        es6: true,
        jest: true,
      },
    },
    plugins: {
      react: eslintPluginReact,
      'jsx-a11y': eslintPluginJSX,
    },
    settings: {
      react: {
        version: 'detect',
      },
    },
    rules: {
      'no-unused-vars': 'off',
      'no-console': ['warn', { 'allow': ['warn', 'error'] }],
      'react/jsx-uses-react': 'off',
      'react/react-in-jsx-scope': 'off',
      'react/prop-types': 'off',
      'react/jsx-key': 'warn',
      'jsx-a11y/alt-text': 'warn',
      'jsx-a11y/anchor-has-content': 'warn',
      'jsx-a11y/aria-role': 'warn',
      'jsx-a11y/label-has-associated-control': 'warn',
      'react/react-in-jsx-scope': 'off',
    },
  },
  {
    ignores: [
      'coverage/**/*',
      'node_modules/**/*',
    ],
  },
];
