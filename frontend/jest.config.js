import path from 'path';

export default {
  moduleFileExtensions: ['js', 'jsx', 'json', 'ts', 'tsx'],
  testEnvironment: "jsdom",
  moduleNameMapper: {
    '\\.svg': path.resolve('./jest/svgMock.js'),
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': path.resolve('./jest/fileMock.js'),
    '\\.(css|less|scss|sss|styl)$': 'identity-obj-proxy',
    'env.config': path.resolve('./jest/fallback.env.config.js'),
    '^@/(.*)$': '<rootDir>/src/$1',
    '^@components/(.*)$': '<rootDir>/src/components/$1',
    '^@services/(.*)$': '<rootDir>/src/services/$1',
    '^@data/(.*)$': '<rootDir>/src/data/$1'
  },
  setupFilesAfterEnv: ["<rootDir>/src/setupTests.js"],
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest',
  },
  coveragePathIgnorePatterns: [
    '/node_modules/',
  ],
  collectCoverage: true,
  coverageReporters: ["json", "lcov", "text", "clover"],
};
