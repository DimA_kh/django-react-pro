export const setBuyerName = (name) => {
    return { type: 'SET_BUYER_NAME', payload: name };
};

export const setPhone = (phone) => {
    return { type: 'SET_PHONE', payload: phone };
};

export const setAddress = (address) => {
    return { type: 'SET_ADDRESS', payload: address };
};
