export const setBasketItems = (items) => {
    return {
        type: 'SET_BASKET_ITEMS',
        payload: items,
    };
};

export const addToBasket = (item) => {
    return {
        type: 'ADD_TO_BASKET',
        payload: item,
    };
};

export const removeFromBasket = (itemId) => {
    return {
        type: 'REMOVE_FROM_BASKET',
        payload: itemId,
    };
};

export const incrementAmount = (itemId) => {
    return {
        type: 'INCREMENT_AMOUNT',
        payload: itemId,
    };
};

export const decrementAmount = (itemId) => {
    return {
        type: 'DECREMENT_AMOUNT',
        payload: itemId,
    };
};

export const clearBasket = () => {
    return {
        type: 'CLEAR_BASKET',
    };
};
