import { put, takeLatest, select } from 'redux-saga/effects';
import { setBasketItems } from '@data/actions/basketActions';
import queryClient from '@services/queryClient';

/**
 * Saga responsible for initializing content data.
 * Waits for the 'FETCH_DATA' action and performs data retrieval and processing.
 *
 * @generator
 * @function
 * @name initializeContentSaga
 */
export function* initializeContentSaga() {
    yield takeLatest('FETCH_DATA', function* () {
        try {
            // Retrieves the cached data for the specified query key from the QueryClient.
            // In this case, it fetches the cached data for the 'products' query.
            const allItems = queryClient.getQueryData(['products']);

            // Function to find an item by its ID
            const findItemById = (itemId) => {
                return allItems.find((item) => item.id === itemId);
            };

            // Filters the basket items based on available quantity in the cached data
            const filteredBasketItems = yield select(state => {
                const basketItems = state.basketReducer.basketItems;
                return basketItems.filter((item) => {
                    const correspondingItem = findItemById(item.id);
                    return correspondingItem?.quantity > 0;
                });
            });

            // Updates the basket items with the latest data and adjusts the quantity if needed
            const updatedBasketItems = filteredBasketItems.map((item) => {
                let newAmount;
                const correspondingItem = findItemById(item.id);
                if (correspondingItem?.quantity < item.amount) {
                    newAmount = correspondingItem.quantity;
                } else {
                    newAmount = item.amount;
                }
                return {
                    id: item.id,
                    title: correspondingItem ? correspondingItem.title : '',
                    image: correspondingItem ? correspondingItem.image : '',
                    price: correspondingItem ? correspondingItem.price : 0,
                    quantity: correspondingItem ? correspondingItem.quantity : 0,
                    amount: newAmount,
                };
            });

            // Dispatches the updated basket items to the Redux store.
            yield put(setBasketItems(updatedBasketItems));
        } catch (error) {
            console.error('Error initializing content:', error);
        }
    });
}
