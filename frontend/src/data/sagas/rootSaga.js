/**
 * Root Saga responsible for combining all sagas used in the application.
 */

import { all } from 'redux-saga/effects';
import { initializeContentSaga } from './initializeContentSaga';

export function* rootSaga() {
    yield all([
        initializeContentSaga(),
    ]);
}
