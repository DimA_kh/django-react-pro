import storage from 'redux-persist/lib/storage';                     // Using localStorage
import expireInTransform from 'redux-persist-transform-expire-in';   // It creates in the localStorage a property with the expiration date of the redux-persist

const expireIn = 30 * 24 * 60 * 60 * 1000;                           // Expire in 30 days 
const expireKey = 'expireBasket';
const expiredReducer = {
    basketItems: [],
};


const persistConfig = {
    key: 'basket',                                                   // The key by which the state will be stored
    storage,
    transforms: [
        expireInTransform(
            expireIn,
            expireKey,
            expiredReducer,
        )
    ],
    whitelist: ['basketReducer'],                                    // List of reducers to save
};

export default persistConfig;
