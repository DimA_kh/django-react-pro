const initialState = {
    basketItems: [],
};

const basketReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_BASKET_ITEMS':
            return {
                ...state,
                basketItems: action.payload,
            };

        case 'ADD_TO_BASKET':
            return {
                ...state,
                basketItems: [...state.basketItems, action.payload],
            };

        case 'REMOVE_FROM_BASKET':
            return {
                ...state,
                basketItems: state.basketItems.filter((item) => item.id !== action.payload),
            };

        case 'INCREMENT_AMOUNT':
            return {
                ...state,
                basketItems: state.basketItems.map((item) =>
                    item.id === action.payload ? { ...item, amount: item.amount + 1 } : item
                ),
            };

        case 'DECREMENT_AMOUNT':
            return {
                ...state,
                basketItems: state.basketItems.map((item) =>
                    item.id === action.payload ? { ...item, amount: item.amount - 1 } : item
                ),
            };

        case 'CLEAR_BASKET':
            return {
                ...state,
                basketItems: [],
            };

        default:
            return state;
    }
};

export default basketReducer;
