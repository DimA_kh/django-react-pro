const initialState = {
    buyerName: '',
    phone: '',
    address: '',
};

const checkoutReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_BUYER_NAME':
            return { ...state, buyerName: action.payload };

        case 'SET_PHONE':
            return { ...state, phone: action.payload };

        case 'SET_ADDRESS':
                return { ...state, address: action.payload };

        default:
            return state;
    }
};

export default checkoutReducer;
