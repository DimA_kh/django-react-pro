import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import persistConfig from '@data/persistConfig';
import basketReducer from '@data/reducers/basketReducer';
import checkoutReducer from '@data/reducers/checkoutReducer';
import { rootSaga } from '@data/sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();

// The root reducer that combines all reducers used in the application.
const rootReducer = combineReducers({
    basketReducer,
    checkoutReducer,
});

// The persisted reducer using Redux Persist.
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Enhance the store with Redux DevTools extension or compose function.
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// The Redux store with middleware and Redux DevTools extension.
export const store = createStore(
    persistedReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(rootSaga);

// The persistor used for persisting the Redux store.
export const persistor = persistStore(store);
