import axios from 'axios';
import AuthService from '@services/auth.service'

export default function authHeader() {
    if (AuthService.isAuthenticated()) {
        // If the token is still valid, set the Authorization header
        axios.defaults.headers.common['Authorization'] = `Bearer ${AuthService.getAccessToken()}`;
    } else {
        // If the user is not logged in, remove the Authorization header
        delete axios.defaults.headers.common['Authorization'];
    }
}
