import React from 'react';
import { Navigate } from 'react-router-dom';
import AuthService from '@services/auth.service';

const UnauthorizedUserRoute = ({ children }) => {
  return !AuthService.isAuthenticated() ? <Navigate to='/login' /> : children;
};

const AuthorizedUserRoute = ({ children }) => {
  return AuthService.isAuthenticated() ? <Navigate to='/' /> : children;
};

export { UnauthorizedUserRoute, AuthorizedUserRoute };
