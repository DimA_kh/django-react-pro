import axios from 'axios';
import AuthService from '@services/auth.service'
import { BASE_URL } from '@/constants';

export const api = axios.create({
    baseURL: BASE_URL,
    withCredentials: true,
});

export const apiWithoutAuthentication = axios.create({
    baseURL: BASE_URL,
});

api.interceptors.request.use(
    async (config) => {
        const accessToken = AuthService.getAccessToken();
        const refreshToken = AuthService.getRefreshToken();
        if (accessToken) {
            config.headers["Authorization"] = 'Bearer ' + accessToken;
        } else if (refreshToken) {
            const response = await axios.post(`${BASE_URL}api/auth/refresh/`, {
                refresh: refreshToken
            });
            const { access } = response.data;
            AuthService.saveAccessToken(access);
            config.headers["Authorization"] = 'Bearer ' + access;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export const fetchItems = () => {
    return apiWithoutAuthentication.get('/api/shop/product/')
    .then(response => {
        return response.data.products;
    })
    .catch(error => {
        console.error('Error fetching items:', error);
        throw error;
    });
};

export const productReservation = (requestData) => {
    return api.post('/api/shop/product/reservation/', requestData)
    .then(response => {
        return response.data.products_reserved;
    })
    .catch(error => {
        console.error('Error fetching items:', error);
        throw error;
    });
};

export const fetchItem = (id) => {
    return apiWithoutAuthentication.get(`/api/shop/product/${id}`)
    .then(response => {
        return response.data.product;
    })
    .catch(error => {
        console.error('Error fetching items:', error);
        throw error;
    });
};

export const fetchUserOrders = () => {
    return api.get('/api/shop/order/')
    .then(response => {
        return response.data.orders;
    })
    .catch(error => {
        console.error('Error fetching orders:', error);
        throw error;
    });
};

export const cancelOrder = (requestData) => {
    return api.post('/api/shop/order/cancel/', requestData)
    .then(response => {
        return response.data.orders;
    })
    .catch(error => {
        console.error('Error cancel order:', error);
        throw error;
    });
};

export const createOrder = (requestData) => {
    return api.post('/api/shop/order/create/', requestData)
    .then(response => {
        return response;
    })
    .catch(error => {
        console.error('Error create order:', error);
        throw error;
    });
};

export const fetchOrderData = (id) => {
    return api.get(`/api/shop/order/${id}`)
    .then(response => {
        return response.data;
    })
    .catch(error => {
        console.error('Error fetch order:', error);
        throw error;
    });
};

export const searchProducts = (query) => {
    return apiWithoutAuthentication.get(`/api/shop/product/search?find=${query}`)
    .then(response => {
        return response.data.hits;
    })
    .catch(error => {
        console.error('Error searching products:', error);
        throw error;
    });
};
