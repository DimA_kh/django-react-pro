import axios from 'axios';
import authHeader from '@services/auth.header';
import AuthService from '@services/auth.service';

jest.mock('../auth.service');

describe('authHeader', () => {
    afterEach(() => {
        delete axios.defaults.headers.common['Authorization'];
        jest.clearAllMocks();
    });

    it('should set Authorization header when user is logged in and token is valid', () => {
        AuthService.isAuthenticated.mockReturnValue(true);
        AuthService.getAccessToken.mockReturnValue('token');
        authHeader();

        expect(axios.defaults.headers.common['Authorization']).toEqual('Bearer token');
    });

    it('should remove Authorization header when user is not logged in', () => {
        AuthService.isAuthenticated.mockReturnValue(false);
        authHeader();

        expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });
});
