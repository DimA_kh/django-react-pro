import axios from 'axios';
import AuthService from '@services/auth.service';

jest.mock('axios');

describe('AuthService', () => {
    afterEach(() => {
        jest.clearAllMocks();
        document.cookie.split(';').forEach(cookie => {
            document.cookie = cookie.replace(/^ +/, '').replace(/=.*/, '=;expires=Thu, 01 Jan 1970 00:00:00 GMT');
        });
    });

    it('should login user and store user data in cookies', async () => {
        const userData = {
            access: 'token',
            refresh: 'refresh_token',
            email: 'test@example.com',
            name: 'Test User',
            id: 1,
            is_superuser: false
        };

        axios.post.mockResolvedValueOnce({ data: userData });
        await AuthService.login('test@example.com', 'password');
        const cookies = document.cookie.split('; ').reduce((acc, cookie) => {
            const [name, value] = cookie.split('=');
            acc[name] = decodeURIComponent(value);
            return acc;
        }, {});

        expect(cookies['shop-access']).toBe(userData.access);
        expect(cookies['shop-refresh']).toBe(userData.refresh);
        expect(cookies['shop-user']).toBe(JSON.stringify({
            email: userData.email,
            name: userData.name,
            id: userData.id,
            is_superuser: userData.is_superuser
        }));
    });

    it('should reject login if response data is empty', async () => {
        axios.post.mockResolvedValueOnce({ data: {} });

        await expect(AuthService.login('test@example.com', 'password')).rejects.toEqual('Empty response received');
    });

    it('should logout user and remove user data from cookies', async () => {
        document.cookie = 'shop-refresh=refresh_token';
        document.cookie = 'shop-access=access_token';
        document.cookie = 'shop-user=' + encodeURIComponent(JSON.stringify({ email: 'test@example.com' }));

        axios.post.mockResolvedValueOnce({});
        await AuthService.logout();
        const cookies = document.cookie.split('; ').reduce((acc, cookie) => {
            const [name, value] = cookie.split('=');
            acc[name] = decodeURIComponent(value);
            return acc;
        }, {});

        expect(cookies['shop-access']).toBeUndefined();
        expect(cookies['shop-refresh']).toBeUndefined();
        expect(cookies['shop-user']).toBeUndefined();
        expect(axios.post).toHaveBeenCalledWith(expect.any(String), {
            refresh: 'refresh_token',
            access: 'access_token',
            user_data: encodeURIComponent(JSON.stringify({ email: 'test@example.com' }))
        });
    });

    it('should handle logout when no refresh token is present', async () => {
        document.cookie = 'shop-access=access_token';
        document.cookie = 'shop-user=' + encodeURIComponent(JSON.stringify({ email: 'test@example.com' }));

        axios.post.mockResolvedValueOnce({});
        await AuthService.logout();
        const cookies = document.cookie.split('; ').reduce((acc, cookie) => {
            const [name, value] = cookie.split('=');
            acc[name] = decodeURIComponent(value);
            return acc;
        }, {});

        expect(cookies['shop-access']).toBeUndefined();
        expect(cookies['shop-refresh']).toBeUndefined();
        expect(cookies['shop-user']).toBeUndefined();
    });

    it('should register user', async () => {
        const userData = { email: 'test@example.com', name: 'Test User', password: 'password' };
        axios.post.mockResolvedValueOnce({});
        await AuthService.register(userData.email, userData.name, userData.password);

        expect(axios.post).toHaveBeenCalledWith(expect.any(String), userData);
    });
});
