import MockAdapter from 'axios-mock-adapter';
import {
    api,
    apiWithoutAuthentication,
    cancelOrder,
    createOrder,
    fetchOrderData,
    fetchItem,
    fetchItems,
    fetchUserOrders,
    productReservation
} from '@services/api';

let mockApi;
let mockForFreeApi;

beforeEach(() => {
    mockApi = new MockAdapter(api);
    mockForFreeApi = new MockAdapter(apiWithoutAuthentication);
});

afterEach(() => {
    mockApi.reset();
    mockForFreeApi.reset();
});

test('fetchItems should retrieve products', async () => {
    const data = { products: [{ id: 1, name: 'Product 1' }] };
    mockForFreeApi.onGet('/api/shop/product/').reply(200, data);
    const response = await fetchItems();

    expect(response).toEqual(data.products);
});

test('productReservation should reserve a product', async () => {
    const requestData = { productId: 1 };
    const data = { products_reserved: [{ id: 1, name: 'Product 1' }] };
    mockApi.onPost('/api/shop/product/reservation/', requestData).reply(200, data);
    const response = await productReservation(requestData);

    expect(response).toEqual(data.products_reserved);
});

test('fetchItem should retrieve a product by id', async () => {
    const id = 1;
    const data = { product: { id: 1, name: 'Product 1' } };
    mockForFreeApi.onGet(`/api/shop/product/${id}`).reply(200, data);
    const response = await fetchItem(id);

    expect(response).toEqual(data.product);
});

test('fetchOrderData should retrieve order data', async () => {
    const id = 1;
    const data = { order: { id: 1, name: 'Order 1' } };
    mockApi.onGet(`/api/shop/order/${id}`).reply(200, data);
    const response = await fetchOrderData(id);

    expect(response).toEqual(data);
});

test('fetchUserOrders should retrieve user orders', async () => {
    const data = { orders: [{ id: 1, name: 'Order 1' }] };
    mockApi.onGet('/api/shop/order/').reply(200, data);
    const response = await fetchUserOrders();

    expect(response).toEqual(data.orders);
});

test('cancelOrder should cancel an order', async () => {
    const requestData = { orderId: 1 };
    const data = { orders: [{ id: 1, name: 'Order 1' }] };
    mockApi.onPost('/api/shop/order/cancel/', requestData).reply(200, data);

    const response = await cancelOrder(requestData);

    expect(response).toEqual(data.orders);
});

test('createOrder should create an order', async () => {
    const requestData = { productId: 1 };
    const data = { order: { id: 1, name: 'Order 1' } };
    mockApi.onPost('/api/shop/order/create/', requestData).reply(200, data);
    const response = await createOrder(requestData);

    expect(response.data).toEqual(data);
});

test('productReservation should handle error', async () => {
    const requestData = { productId: 1 };
    mockApi.onPost('/api/shop/product/reservation/', requestData).reply(500);

    await expect(productReservation(requestData)).rejects.toThrow();
});

test('fetchItems', async () => {
    mockApi.onGet('/api/shop/product/').reply(500);

    await expect(fetchItems()).rejects.toThrow();
});

test('fetchItem should handle error', async () => {
    const id = 1;
    mockForFreeApi.onGet(`/api/shop/product/${id}`).reply(500);

    await expect(fetchItem(id)).rejects.toThrow();
});

test('fetchUserOrders should handle error', async () => {
    mockApi.onGet('/api/shop/order/').reply(500);

    await expect(fetchUserOrders()).rejects.toThrow();
});

test('cancelOrder should handle error', async () => {
    const requestData = { orderId: 1 };
    mockApi.onPost('/api/shop/order/cancel/', requestData).reply(500);

    await expect(cancelOrder(requestData)).rejects.toThrow();
});

test('createOrder should handle error', async () => {
    const requestData = { productId: 1 };
    mockApi.onPost('/api/shop/order/create/', requestData).reply(500);

    await expect(createOrder(requestData)).rejects.toThrow();
});

test('fetchOrderData should handle error', async () => {
    const id = 1;
    mockApi.onGet(`/api/shop/order/${id}`).reply(500);

    await expect(fetchOrderData(id)).rejects.toThrow();
});
