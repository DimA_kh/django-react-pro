import { QueryClient } from '@tanstack/react-query';

/**
 * Creates a QueryClient with customized default options for queries and mutations.
 * 
 * @const {QueryClient} queryClient - The QueryClient instance with default options.
 * @defaultOptions {Object} - Default settings for queries and mutations.
 * @defaultOptions.queries {Object} - Default settings for queries.
 * @staleTime {number} - Time (in milliseconds) that query data remains fresh.
 * @retry {number} - Number of retry attempts for failed queries.
 * @retryDelay {function} - Function to determine the delay between retry attempts.
 * @cacheTime {number} - Time (in milliseconds) that cached query data remains in memory.
 * @refetchOnWindowFocus {boolean} - If true, refetch data when window gains focus.
 * @refetchOnReconnect {boolean} - If true, refetch data when internet reconnects.
 * @refetchOnMount {boolean} - If true, refetch data when the component mounts.
 * 
 * @defaultOptions.mutations {Object} - Default settings for mutations.
 * @retry {number} - Number of retry attempts for failed mutations.
 * @retryDelay {number} - Delay (in milliseconds) between retry attempts for mutations.
 */
const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 10 * 60 * 1000,
            retry: 2,
            retryDelay: (attempt) => Math.min(1000 * 2 ** attempt, 30000),
            cacheTime: 10 * 60 * 1000,
            refetchOnWindowFocus: false,
            refetchOnReconnect: false,
            refetchOnMount: false,
        },
        mutations: {
            retry: 1,
            retryDelay: 1000,
        }
    }
});

export default queryClient;
