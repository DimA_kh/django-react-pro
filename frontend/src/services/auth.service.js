import axios from "axios";
import { BASE_URL } from '@/constants';

class AuthService {
    onLogoutSuccess() {
        window.location.reload();
    }

    async login(email, password) {
        return axios
            .post(BASE_URL + "api/auth/login/", {
                email,
                password
            })
            .then(response => {
                if (response.data.access) {
                    const user = response.data;
                    const userData = JSON.stringify({
                        email: user.email,
                        name: user.name,
                        id: user.id,
                        is_superuser: user.is_superuser,
                    });
                    // Record user data in cookies
                    document.cookie = `shop-user=${encodeURIComponent(userData)}; path=/; max-age=${20 * 86400}`;
                    document.cookie = `shop-access=${user.access}; path=/; max-age=${60}`;
                    document.cookie = `shop-refresh=${user.refresh}; path=/; max-age=${20 * 86400}`;
                    return response.data;
                }
                return Promise.reject('Empty response received');
            });
    }

    logout() {
        const refresh = this.getCookie('shop-refresh');
        const access = this.getCookie('shop-access');
        const userData = this.getCookie('shop-user');

        if (refresh) {
            axios.post(BASE_URL + "api/auth/logout/", {
                refresh: refresh,
                access: access,
                user_data: userData,

            })
            .then(() => {
                this.deleteCookie('shop-access');
                this.deleteCookie('shop-refresh');
                this.deleteCookie('shop-user');
                this.onLogoutSuccess();
            })
            .catch(error => {
                console.error("Error while logout:", error);
            });
        } else {
            this.deleteCookie('shop-access');
            this.deleteCookie('shop-refresh');
            this.deleteCookie('shop-user');
            this.onLogoutSuccess();
        }
    }

    passwordReset(email) {
        return axios.post(BASE_URL + 'api/auth/password-reset/', { email });
    } 

    resetPasswordConfirm(uid, token, newPassword, confirmPassword) {
        return axios.put(
            BASE_URL + 'api/auth/password-reset-confirm/',
            {
                uid: uid,
                token: token,
                new_password: newPassword,
                confirm_password: confirmPassword,
            }
        );
    }

    async register(email, name, password) {
        try {
            const response = await axios.post(BASE_URL + "api/auth/register/", {
                email,
                name,
                password
            });
            return response.data;
        } catch (error) {
            if (error.response && (error.response.status === 400 || error.response.status === 409)) {
                return { error: error.response.data.message };
            } else {
                return { error: error.message || 'An error occurred during registration' };
            }
        }
    }

    getCurrentUser() {
        const userData = this.getCookie('shop-user');
        if (userData) {
            try {
                const parsedUserData = JSON.parse(decodeURIComponent(userData));
                const user = {
                    email: parsedUserData.email,
                    name: parsedUserData.name,
                    id: parsedUserData.id,
                    is_superuser: parsedUserData.is_superuser,
                };
                return user;
            } catch (error) {
                console.error("Error parsing user data from cookie:", error);
                return null;
            }
        }
        return null;
    }

    isAuthenticated() {
        return !!this.getRefreshToken();
    }

    getRefreshToken() {
        const refreshToken = this.getCookie('shop-refresh');
        return refreshToken ? refreshToken : null;
    }

    getAccessToken() {
        const accessToken = this.getCookie('shop-access');
        return accessToken ? accessToken : null;
    }

    saveAccessToken(token) {
        document.cookie = `shop-access=${token}; path=/; max-age=${60}`;
    }

    getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
        return null;
    }

    deleteCookie(name) {
        document.cookie = `${name}=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
    }
}

export default new AuthService();
