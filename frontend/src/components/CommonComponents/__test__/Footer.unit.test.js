import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Footer from '@components/CommonComponents/Footer';

describe('Footer Component', () => {
    it('should render without crashing', () => {
        const { container } = render(
            <BrowserRouter>
                <Footer />
            </BrowserRouter>
        );
        expect(container).toBeInTheDocument();
    });

    it('should display the correct text and current year', () => {
        const currentYear = new Date().getFullYear();
        const { getByText } = render(
            <BrowserRouter>
                <Footer />
            </BrowserRouter>
        );
        expect(getByText(`© ${currentYear} Welcome to our store`)).toBeInTheDocument();
    });
});
