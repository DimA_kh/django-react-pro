import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import Login from '@components/CommonComponents/login';
import AuthService from '@services/auth.service';

jest.mock('@services/auth.service', () => ({
    login: jest.fn(),
}));

describe('Login', () => {
    beforeEach(() => {
        AuthService.login.mockClear();
    });

    it('should match the snapshot', () => {
        const { asFragment } = render(
            <Router>
                <Login />
            </Router>
        );
        expect(asFragment()).toMatchSnapshot();
    });

    it('should call AuthService.login with the email and password when the form is submitted', async () => {
        AuthService.login.mockResolvedValue();
        const { getByLabelText, getByText } = render(
            <Router>
                <Login />
            </Router>
        );

        fireEvent.change(getByLabelText('Email'), { target: { value: 'test@example.com' } });
        fireEvent.change(getByLabelText('Password'), { target: { value: 'testPassword' } });

        fireEvent.submit(getByText('Sign In'));
        await waitFor(() => {
            expect(AuthService.login).toHaveBeenCalledTimes(1);
            expect(AuthService.login).toHaveBeenCalledWith('test@example.com', 'testPassword');
        });
    });

    it('should display error message when login fails', async () => {
        AuthService.login.mockRejectedValue({ response: { status: 401 } });
        const { getByLabelText, getByText, queryByText } = render(
            <Router>
                <Login />
            </Router>
        );

        fireEvent.change(getByLabelText('Email'), { target: { value: 'test@example.com' } });
        fireEvent.change(getByLabelText('Password'), { target: { value: 'testPassword' } });

        fireEvent.click(getByText('Sign In'));
        await waitFor(() => {
            const errorElement = queryByText('Incorrect password. Please try again.');
            expect(errorElement).not.toBeNull();
            expect(errorElement).toBeInTheDocument();
        });

        expect(screen.getByRole('button', { name: 'Sign In' })).not.toBeDisabled();
    });
});
