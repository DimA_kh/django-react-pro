import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import Register from '@components/CommonComponents/register';
import AuthService from '@services/auth.service';

jest.mock('@services/auth.service', () => ({
    register: jest.fn(),
}));

describe('Register', () => {
    beforeEach(() => {
        AuthService.register.mockClear();
    });

    it('should match the snapshot', () => {
        const { asFragment } = render(<Register />);
        expect(asFragment()).toMatchSnapshot();
    });

    it('should call AuthService.register with the name, email, and password when the form is submitted', async () => {
        AuthService.register.mockResolvedValue({
            response: {
                status: 201,
                data: {}
            }
        });
        AuthService.login = jest.fn().mockResolvedValue();
        const { getByLabelText, getByText } = render(<Register />);
        fireEvent.change(getByLabelText('Name'), { target: { value: 'Freddy Kruger' } });
        fireEvent.change(getByLabelText('Email'), { target: { value: 'test@example.com' } });
        fireEvent.change(getByLabelText('Password'), { target: { value: 'testPassword' } });
        fireEvent.click(getByText('Sign Up'));

        await waitFor(() => {
            expect(AuthService.register).toHaveBeenCalledTimes(1);
            expect(AuthService.login).toHaveBeenCalledTimes(1);
            expect(AuthService.register).toHaveBeenCalledWith('test@example.com', 'Freddy Kruger', 'testPassword');
        });
    });
});
