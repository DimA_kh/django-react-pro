import React from 'react';
import { render } from '@testing-library/react';
import ShowAlert from '@components/CommonComponents/ShowAlert';

describe('ShowAlert Component', () => {
    it('ShowAlert should render without crashing', () => {
        const { container } = render(
            <ShowAlert
                onShowAlert={null}
                onHideAlert={false}
            />
        );
        expect(container).toBeInTheDocument();
    });

    it('ShowAlert should pass props correctly', () => {
        const { getByText } = render(
            <ShowAlert
                onShowAlert={'Apple added to basket'}
                onHideAlert={false}
            />
        );
        expect(getByText('Apple added to basket')).toBeInTheDocument();
    });
});
