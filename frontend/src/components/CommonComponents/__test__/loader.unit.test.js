import React from 'react';
import { render, screen } from '@testing-library/react';
import Loader from '@components/CommonComponents/loader';

describe('Loader', () => {
    it('should render loading text', () => {
        render(<Loader />);
        expect(screen.getByText('Loading...')).toBeInTheDocument();
    });

    it('should have loader class', () => {
        render(<Loader />);
        const loader = screen.getByTestId('test-loader');
        expect(loader).toHaveClass('loader');
    });
});
