import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { BASE_URL } from '@/constants';
import Header from '@components/CommonComponents/Header';
import AuthService from '@services/auth.service';

jest.mock('@services/auth.service', () => ({
    getCurrentUser: jest.fn(),
    isAuthenticated: jest.fn(),
    logout: jest.fn(),
}));

jest.mock('@components/ProductComponents/ProductSearch', () => {
    return function ProductSearch() {
        return <div data-testid="mock-product-search">ProductSearch component</div>;
    };
});

describe('Header Component', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render without crashing', () => {
        AuthService.getCurrentUser.mockImplementation(() => ({ name: 'John', is_superuser: true }));
        const { container } = render(
            <BrowserRouter>
                <Header />
            </BrowserRouter>
        );
        expect(container).toBeInTheDocument();
    });

    it('should display "Home" and "My orders" buttons', () => {
        AuthService.getCurrentUser.mockImplementation(() => ({ name: 'John', is_superuser: false }));
        AuthService.isAuthenticated.mockReturnValue(true);

        const { getByText } = render(
            <BrowserRouter>
                <Header />
            </BrowserRouter>
        );

        const homeButton = getByText('Home');
        const ordersButton = getByText('My orders');

        expect(homeButton).toBeInTheDocument();
        expect(ordersButton).toBeInTheDocument();

        fireEvent.click(homeButton);
        fireEvent.click(ordersButton);
    });

    it('should display "Admin panel" button for superusers', () => {
        AuthService.getCurrentUser.mockImplementation(() => ({ name: 'John', is_superuser: true }));
        AuthService.isAuthenticated.mockReturnValue(true);

        render(
            <BrowserRouter>
                <Header />
            </BrowserRouter>
        );

        const adminPanelButton = screen.getByText('Admin panel');
        expect(adminPanelButton).toBeInTheDocument();
    
        const mockWindowOpen = jest.fn();
        global.window.open = mockWindowOpen;
        fireEvent.click(adminPanelButton);

        expect(mockWindowOpen).toHaveBeenCalledTimes(1);
        expect(mockWindowOpen).toHaveBeenCalledWith(`${BASE_URL}admin/`, '_blank');
    });

    it('should not display "Admin panel" button for regular users', () => {
        AuthService.getCurrentUser.mockImplementation(() => ({ name: 'John', is_superuser: false }));
        AuthService.isAuthenticated.mockReturnValue(true);

        const { queryByText } = render(
            <BrowserRouter>
                <Header />
            </BrowserRouter>
        );
        const adminButton = queryByText('Admin panel');
        expect(adminButton).toBeNull();
    });

    it('should display "Login" and "Register" buttons for non-logged-in users', () => {
        AuthService.getCurrentUser.mockImplementation(() => null);
        AuthService.isAuthenticated.mockReturnValue(false);

        const { getByTestId } = render(
            <BrowserRouter>
                <Header />
            </BrowserRouter>
        );
        const loginButton = getByTestId('login-button-test');
        const registerButton = getByTestId('register-button-test');

        expect(loginButton).toBeInTheDocument();
        expect(registerButton).toBeInTheDocument();

        fireEvent.click(loginButton);
        fireEvent.click(registerButton);
    });

    it('should call `AuthService.logout` when "Sign Out" button is clicked', async () => {
        AuthService.getCurrentUser.mockImplementation(() => ({ name: 'John', is_superuser: false }));
        AuthService.isAuthenticated.mockReturnValue(true);

        const { getByTestId } = render(
            <BrowserRouter>
                <Header />
            </BrowserRouter>
        );
        const logoutButton = getByTestId('logout-button-test');
        expect(logoutButton).toBeInTheDocument();

        fireEvent.click(logoutButton);
        await waitFor(() => {
            expect(AuthService.logout).toHaveBeenCalled();
        });
    });

    it('should not display "Logout" button for non-logged-in users', () => {
        AuthService.getCurrentUser.mockImplementation(() => null);
        AuthService.isAuthenticated.mockReturnValue(false);

        const { queryByTestId } = render(
            <BrowserRouter>
                <Header />
            </BrowserRouter>
        );
        const logoutButton = queryByTestId('logout-button-test');
        expect(logoutButton).toBeNull();
    });
});
