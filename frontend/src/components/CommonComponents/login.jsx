import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { isEmail } from 'validator';
import { Button, TextField, IconButton, InputAdornment } from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import AuthService from '@services/auth.service';

const Login = () => {
  const navigate = useNavigate();
  const [formValues, setFormValues] = useState({
    email: "",
    password: ""
  });
  const [isLabelVisible, setIsLabelVisible] = useState({
    email: true,
    password: true
  });
  const [emailErrors, setEmailErrors] = useState("");
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword((prev) => !prev);
  };

  const handleMouseDownPassword = (e) => {
    e.preventDefault();
  };

  const handleFocus = (field) => {
    setIsLabelVisible((prevState) => ({ ...prevState, [field]: false }));
  };

  const handleBlur = (field) => {
    if (!formValues[field]) {
      setIsLabelVisible((prevState) => ({ ...prevState, [field]: true }));
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues((prevState) => ({ ...prevState, [name]: value }));
    setEmailErrors("");
  };

  const handleLogin = (e) => {
    e.preventDefault();
    setMessage("");
    setLoading(true);

    const { email, password } = formValues;
    if (!isEmail(email)) {
      setEmailErrors("Invalid email format");
      setLoading(false);
      return;
    }

    AuthService.login(email, password)
      .then(() => {
        window.location.href = '/';
      })
      .catch((error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        console.error(error.toString());
        if (error.response && error.response.status === 401) {
          setMessage("Incorrect password. Please try again.");
        } else {
          setMessage(resMessage);
        }
        setLoading(false);
      });
  };

  const handleRegisterRedirect = () => {
    navigate('/register');
  };

  const handlePasswordResetRedirect = () => {
    navigate('/password-reset');
  };

  return (
    <div className="container mt-4 mb-4">
      <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <div className="card card-container" style={{ backgroundColor: "rgb(122, 155, 139)" }}>
            <form onSubmit={handleLogin}>
              <div className="form-group text-center">
                <label htmlFor="email">Email</label>
                <TextField
                  id="email"
                  type="text"
                  className="form-control"
                  label={isLabelVisible.email ? "Email" : ""}
                  onFocus={() => handleFocus("email")}
                  onBlur={() => handleBlur("email")}
                  name="email"
                  value={formValues.email}
                  onChange={handleChange}
                  variant="outlined"
                  fullWidth
                  required
                  error={!!emailErrors}
                  helperText={emailErrors}
                />
              </div>
              <div className="form-group text-center">
                <label htmlFor="password">Password</label>
                <TextField
                  id="password"
                  type={showPassword ? "text" : "password"}
                  className="form-control"
                  label="Password"
                  name="password"
                  value={formValues.password}
                  onChange={handleChange}
                  variant="outlined"
                  fullWidth
                  required
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
              <div className="form-group text-center mt-2 mb-2">
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  disabled={loading}
                >
                  {loading && (
                    <span className="spinner-border spinner-border-sm"></span>
                  )}
                  <span>Sign In</span>
                </Button>
              </div>
              {message && (
                <div className="form-group">
                  <div className="alert alert-danger" role="alert">
                    {message}
                  </div>
                </div>
              )}
              <div className="form-group text-center mt-2 mb-2">
                <button
                  onClick={handleRegisterRedirect}
                  style={{ background: 'none', border: 'none', cursor: 'pointer' }}
                >
                  Create New Account
                </button>
              </div>
              <div className="form-group text-center mt-2 mb-2">
                <button
                  onClick={handlePasswordResetRedirect}
                  style={{ background: 'none', border: 'none', cursor: 'pointer' }}
                >
                  Forgot Password?
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="col-md-4"></div>
      </div>
    </div>
  );
};

export default Login;
