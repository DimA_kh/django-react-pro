import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Container, Col, Navbar, Nav, NavItem, Row } from 'reactstrap';
import { BASE_URL } from '@/constants';
import AuthService from '@services/auth.service'
import ProductSearch from  '@components/ProductComponents/ProductSearch';
import '@components/CommonComponents/Header.css';

const Header = () => {
  const navigate = useNavigate();
  const currentUser = AuthService.getCurrentUser();
  const [activeLink, setActiveLink] = useState('');

  const handleAdminPanelClick = () => {
    window.open(BASE_URL + 'admin/', '_blank');
  };

  const handleDashboardClick = () => {
    setActiveLink('seller-dashboard');
    window.location.href = BASE_URL + 'seller-dashboard/';
  };

  const handleLogout = () => {
    AuthService.logout();
    navigate('/');
  };

  const handleNavigate = (path) => {
    setActiveLink(path);
    navigate(path);
  };

  return (
    <header data-testid="test-header">
      <Navbar id="nav" light expand="xs" className="border-bottom" style={{ height: 80 }}>
        <Container>
          <Row className="position-relative w-100 align-items-center g-0">
            <Col className="d-none d-lg-flex justify-content-start">
              <Nav className="mrx-auto" navbar>           
                <NavItem className="d-flex align-items-center">
                  <button 
                    className={`nav-link ${activeLink === '/' ? 'active' : ''}`}
                    onClick={() => handleNavigate("/")}
                  >
                    Home
                  </button>
                </NavItem>
                <NavItem className="d-flex align-items-center">
                  {(AuthService.isAuthenticated()) && (
                    <button className={`nav-link ${activeLink === '/order' ? 'active' : ''}`} onClick={() => handleNavigate("/order")}>My orders</button>
                  )}
                </NavItem>
                <NavItem className="d-flex align-items-center">
                  {(AuthService.isAuthenticated() && (currentUser.is_superuser || currentUser.is_staff)) && (
                    <button className={`nav-link ${activeLink === 'seller-dashboard' ? 'active' : ''}`} onClick={handleDashboardClick}>
                      Dashboard
                    </button>
                  )}
                </NavItem>
                <NavItem className="d-flex align-items-center">
                  {(AuthService.isAuthenticated() && currentUser.is_superuser) ? (
                    <>
                      <button className={`nav-link ${activeLink === 'admin-panel' ? 'active' : ''}`} onClick={handleAdminPanelClick}>
                        Admin panel
                      </button>
                    </>
                  ) : null}
                </NavItem>
              </Nav>
            </Col>

            <Col className="d-none d-lg-flex justify-content-end">
              <ProductSearch />
            </Col>

            <Col className="d-none d-lg-flex justify-content-end">
            {(AuthService.isAuthenticated()) ? (
              <>
                <h4 id="user">
                  Hello,&nbsp;
                  {currentUser && (
                    <button
                      className="profile-link"
                      onClick={() => navigate("/profile")}
                    >
                      {currentUser.name}
                    </button>
                    )}
                </h4>
                <Button color="outline-danger" size="sm" inline="true" data-testid="logout-button-test" onClick={handleLogout}>
                  Sign Out
                </Button>
              </>
            ) : (
              <>
                <Button color="primary" data-testid="login-button-test">
                  <Link to={"/login"} className="nav-link">
                    Sign In
                  </Link>
                </Button>
                <Button id="register" color="primary" data-testid="register-button-test">
                  <Link to={"/register"} className="nav-link">
                    Sign Up
                  </Link>
                </Button>
              </>
            )}
            </Col>
          </Row>
        </Container>
      </Navbar>
    </header>
  );
}

export default Header;
