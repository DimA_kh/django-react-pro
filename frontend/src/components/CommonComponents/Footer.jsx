import React from 'react';
import '@components/CommonComponents/Footer.css';

function Footer() {
    return (
        <footer className="page-footer" data-testid="test-footer">
            <div className="container">
                © {new Date().getFullYear()} Welcome to our store
            </div>
        </footer>
    )
}

export default Footer;
