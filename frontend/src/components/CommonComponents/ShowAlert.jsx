import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import '@components/CommonComponents/ShowAlert.css';

function ShowAlert(props) {
    useEffect(() => {
        const timeoutId = setTimeout(props.onHideAlert, 2600);
        return () => clearTimeout(timeoutId);
    }, [props.onShowAlert]);

    return <div className="show-alert">{props.onShowAlert}</div>;
}

ShowAlert.propTypes = {
    onHideAlert: PropTypes.func.isRequired,
    onShowAlert: PropTypes.string.isRequired
};

export default ShowAlert;

