import React from 'react';
import '@components/CommonComponents/loader.css';

function Loader() {
    return (
        <div className="loader-container">
            <div className="loader" data-testid="test-loader"></div>
            <p className="loader-text">Loading...</p>
        </div>
    );
}

export default Loader;
