import React, { useState } from 'react';
import { isEmail } from 'validator';
import { Button, TextField } from '@mui/material';
import AuthService from '@services/auth.service';

const PasswordResetRequest = () => {
  const [email, setEmail] = useState("");
  const [emailErrors, setEmailErrors] = useState("");
  const [message, setMessage] = useState("");
  const [messageType, setMessageType] = useState("error");
  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    setEmail(e.target.value);
    setEmailErrors("");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    setMessage("");

    if (!isEmail(email)) {
      setEmailErrors("Invalid email format");
      setLoading(false);
      return;
    }

    AuthService.passwordReset(email)
      .then(() => {
        setMessageType("success");
        setMessage(`Password reset link has been sent to your email ${email}`);
        setLoading(false);
      })
      .catch((error) => {
        const resMessage = error.response.data.message || error.message;
        setMessageType("error");
        setMessage(resMessage);
        setLoading(false);
      });
  };

  return (
    <div className="container mt-4 mb-4">
      <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <div className="card card-container" style={{ backgroundColor: "rgb(122, 155, 139)" }}>
            {message ? (
              <div className="form-group text-center">
                <div className={`alert ${messageType === 'error' ? 'alert-danger' : 'alert-success'}`} role="alert">
                  {message}
                </div>
              </div>
            ) : (
              <form onSubmit={handleSubmit}>
                <div className="form-group text-center">
                  <label htmlFor="email">Email</label>
                  <TextField
                    id="email"
                    type="email"
                    className="form-control"
                    label="Email"
                    name="email"
                    value={email}
                    onChange={handleChange}
                    variant="outlined"
                    fullWidth
                    required
                    error={!!emailErrors}
                    helperText={emailErrors}
                  />
                </div>
                <div className="form-group text-center mt-2 mb-2">
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    disabled={loading}
                  >
                    {loading && (
                      <span className="spinner-border spinner-border-sm"></span>
                    )}
                    <span>Reset Password</span>
                  </Button>
                </div>
              </form>
            )}
          </div>
        </div>
        <div className="col-md-4"></div>
      </div>
    </div>
  );
};

export default PasswordResetRequest;
