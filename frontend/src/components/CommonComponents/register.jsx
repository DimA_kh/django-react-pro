import React, { useState } from 'react';
import { isEmail } from 'validator';
import { Button, TextField, IconButton, InputAdornment } from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import AuthService from '@services/auth.service';

const Register = () => {
  const [formValues, setFormValues] = useState({
    name: "",
    email: "",
    password: ""
  });
  const [emailErrors, setEmailErrors] = useState("");
  const [isLabelVisible, setIsLabelVisible] = useState({
    name: true,
    email: true,
    password: true
  });
  const [successful, setSuccessful] = useState(false);
  const [message, setMessage] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword((prev) => !prev);
  };

  const handleMouseDownPassword = (e) => {
    e.preventDefault();
  };

  const handleFocus = (field) => {
    setIsLabelVisible((prevState) => ({ ...prevState, [field]: false }));
  };

  const handleBlur = (field) => {
    if (!formValues[field]) {
      setIsLabelVisible((prevState) => ({ ...prevState, [field]: true }));
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues((prevState) => ({ ...prevState, [name]: value }));
    setEmailErrors("");
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    setMessage("");
    setSuccessful(false);

    const { name, email, password } = formValues;
    if (!isEmail(email)) {
      setEmailErrors("Invalid email format");
      return;
    }

    const registrationResult = await AuthService.register(email, name, password);
    if (!registrationResult.error) {
      await AuthService.login(email, password);
      window.location.replace("/");
    } else {
      setSuccessful(false);
      setMessage(registrationResult.error);
    }
  };

  return (
    <div className="container mt-4 mb-4">
      <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <div className="card card-container" style={{ backgroundColor: "rgb(122, 155, 139)" }}>
            {!successful && (
              <form onSubmit={handleRegister}>
                <div className="form-group text-center">
                  <label htmlFor="name">Name</label>
                  <TextField
                    id="name"
                    type="text"
                    className="form-control"
                    label={isLabelVisible.name ? "Name" : ""}
                    onFocus={() => handleFocus("name")}
                    onBlur={() => handleBlur("name")}
                    name="name"
                    value={formValues.name}
                    onChange={handleChange}
                    variant="outlined"
                    fullWidth
                    required
                  />
                </div>
                <div className="form-group text-center">
                  <label htmlFor="email">Email</label>
                  <TextField
                    id="email"
                    type="text"
                    className="form-control"
                    label={isLabelVisible.email ? "Email" : ""}
                    onFocus={() => handleFocus("email")}
                    onBlur={() => handleBlur("email")}
                    name="email"
                    value={formValues.email}
                    onChange={handleChange}
                    variant="outlined"
                    fullWidth
                    required
                    error={!!emailErrors}
                    helperText={emailErrors}
                  />
                </div>
                <div className="form-group text-center">
                  <label htmlFor="password">Password</label>
                  <TextField
                    id="password"
                    type={showPassword ? "text" : "password"}
                    className="form-control"
                    label="Password"
                    name="password"
                    value={formValues.password}
                    onChange={handleChange}
                    variant="outlined"
                    fullWidth
                    required
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                          >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
                <div className="form-group text-center mt-2 mb-2">
                  <Button variant="contained" color="primary" type="submit">Sign Up</Button>
                </div>
              </form>
            )}
            {message && (
              <div className="form-group">
                <div
                  className={
                    successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {message}
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="col-md-4"></div>
      </div>
    </div>
  );
};

export default Register;
