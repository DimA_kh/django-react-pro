import React, { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Button, TextField, IconButton, InputAdornment } from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import AuthService from '@services/auth.service';

const ResetPassword = () => {
  const { uid, token } = useParams();
  const navigate = useNavigate();
  const [formValues, setFormValues] = useState({
    newPassword: "",
    confirmPassword: ""
  });
  const [message, setMessage] = useState("");
  const [messageType, setMessageType] = useState("error");
  const [showNewPassword, setNewShowPassword] = useState(false);
  const [showConfirmPassword, setConfirmShowPassword] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleClickShowNewPassword = () => {
    setNewShowPassword((prev) => !prev);
  };

  const handleMouseDownNewPassword = (e) => {
    e.preventDefault();
  };

  const handleClickShowConfirmPassword = () => {
    setConfirmShowPassword((prev) => !prev);
  };

  const handleMouseDownConfirmPassword = (e) => {
    e.preventDefault();
  };

  const handleResetPassword = (e) => {
    e.preventDefault();
    setMessage("");

    const { newPassword, confirmPassword } = formValues;
    if (newPassword !== confirmPassword) {
      setMessageType("error");
      setMessage("Passwords do not match");
      return;
    }

    AuthService.resetPasswordConfirm(uid, token, newPassword, confirmPassword)
      .then(() => {
        setMessageType("success");
        setMessage("Your new password has been successfully set. You can now log in with your new password.");
        setTimeout(() => {
          navigate('/login');
        }, 4000);
      })
      .catch((error) => {
        const resMessage = error.response.data.message || error.message;
        setMessageType("error");
        setMessage(resMessage);
      });
  };

  return (
    <div className="container mt-4 mb-4">
      <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <div className="card card-container" style={{ backgroundColor: "rgb(122, 155, 139)" }}>
            <form onSubmit={handleResetPassword}>
              <div className="form-group text-center">
                <label htmlFor="newPassword">New Password</label>
                <TextField
                  id="newPassword"
                  type={showNewPassword ? "text" : "password"}
                  className="form-control"
                  name="newPassword"
                  value={formValues.newPassword}
                  onChange={handleChange}
                  variant="outlined"
                  fullWidth
                  required
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowNewPassword}
                          onMouseDown={handleMouseDownNewPassword}
                        >
                          {showNewPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
              <div className="form-group text-center">
                <label htmlFor="confirmPassword">Confirm Password</label>
                <TextField
                  id="confirmPassword"
                  type={showConfirmPassword ? "text" : "password"}
                  className="form-control"
                  name="confirmPassword"
                  value={formValues.confirmPassword}
                  onChange={handleChange}
                  variant="outlined"
                  fullWidth
                  required
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowConfirmPassword}
                          onMouseDown={handleMouseDownConfirmPassword}
                        >
                          {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
              <div className="form-group text-center mt-2 mb-2">
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                >
                  Set New Password
                </Button>
              </div>
              {message && (
                <div className="form-group">
                  <div className={`alert ${messageType === 'error' ? 'alert-danger' : 'alert-success'}`} role="alert">
                    {message}
                  </div>
                </div>
              )}
            </form>
          </div>
        </div>
        <div className="col-md-4"></div>
      </div>
    </div>
  );
};

export default ResetPassword;
