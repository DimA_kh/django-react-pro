
import React from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@mui/material';
import { handleCancelOrderOnPageLeave } from '@components/OrderComponents/utils';

function CloseOrderModal({ orderID, open, setModalOpen }) {
    const navigate = useNavigate();

    const handleConfirmClose = async () => {
        await handleCancelOrderOnPageLeave(orderID, navigate);
        setModalOpen(false);
    };

    return (
        <Dialog open={open} onClose={() => setModalOpen(false)}>
            <DialogTitle>Close Order</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Are you sure you want to close this order?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setModalOpen(false)} color="primary">
                    No
                </Button>
                <Button onClick={() => handleConfirmClose()} color="secondary">
                    Yes
                </Button>
            </DialogActions>
        </Dialog>
    );
}

CloseOrderModal.propTypes = {
    orderID: PropTypes.string,
    open: PropTypes.bool,
    setModalOpen: PropTypes.func,
};

export default CloseOrderModal;
