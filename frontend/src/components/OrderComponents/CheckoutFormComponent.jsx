import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import {
    Button,
    Grid,
    TextField,
    Typography,
} from '@mui/material';
import { createOrder } from '@services/api';
import { clearBasket } from '@data/actions/basketActions';
import { setBuyerName, setPhone, setAddress } from '@data/actions/checkoutActions';
import { handleCancelOrderOnPageLeave } from '@components/OrderComponents/utils';
import LiqPayComponent from '@components/OrderComponents/LiqPayComponent';
import CloseOrderModal from '@components/OrderComponents/ConfirmCloseOrder';
import '@components/OrderComponents/OrderPage.css';
import '@components/OrderComponents/TimerStyles.css';

function CheckoutFormComponent({ total, buyerName, phone, address }) {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [orderId, setOrderId] = useState(null);
    const [signature, setSignature] = useState(null);
    const [data, setData] = useState(null);
    const [paymentSuccessful, setPaymentSuccessful] = useState(false);
    const [errorBuyerName, setErrorBuyerName] = useState(false);
    const [errorPhone, setErrorPhone] = useState(false);
    const [errorAddress, setErrorAddress] = useState(false);
    const tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000);
    const [selectedDate, setSelectedDate] = useState(tomorrow);
    const [selectedTime, setSelectedTime] = useState(new Date().setHours(11, 0));
    const [timer, setTimer] = useState(600);
    const [showLiqPay, setShowLiqPay] = useState(true);
    const [expired, setExpired] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);
    const phoneRegex = /^(\+[0-9]*)?$/;

    const handleBuyerNameChange = (buyerName) => {
        dispatch(setBuyerName(buyerName.target.value));
    };

    const handlePhoneChange = (phone) => {
        if (phoneRegex.test(phone.target.value)) {
            dispatch(setPhone(phone.target.value));
        }
    };

    const handleAddressChange = (address) => {
        dispatch(setAddress(address.target.value));
    };

    const handleDateChange = (date) => {
        const currentDate = new Date();
        if (date > currentDate) {
            setSelectedDate(date);
        }
    };

    const handleTimeChange = (newTime) => {
        const [hours, minutes] = newTime.split(':');
        const time = new Date(1970, 0, 1, hours, minutes);
        if (time >= new Date(1970, 0, 1, 9, 0) && time <= new Date(1970, 0, 1, 18, 0)) {
            setSelectedTime(time);
        }
    };

    useEffect(() => {
        const interval = setInterval(() => {
            setTimer(prevTimer => {
                if (prevTimer === 0) {
                    clearInterval(interval);
                    setExpired(true);
                    setShowLiqPay(false);
                    if (orderId) {
                        handleCancelOrderOnPageLeave(orderId, navigate);
                    }
                    return 0;
                }
                return prevTimer - 1;
            });
        }, 1000);
        return () => clearInterval(interval);
    }, [orderId]);

    const handlePayNow = async () => {
        if (!buyerName || !phone || !address) {
            setErrorBuyerName(buyerName ? false : true);
            setErrorPhone(phone ? false : true);
            setErrorAddress(address ? false : true);
            setShowLiqPay(true);
            return;
        }
        try {
            const requestData = {
                buyerName,
                phone,
                address,
                selectedDate: format(selectedDate, 'yyyy-MM-dd'),
                selectedTime: format(selectedTime, 'HH:mm'),
                total,
            };
            const response = await createOrder(requestData);
            setSignature(response.data.signature);
            setData(response.data.payment_data);
            setOrderId(response.data.order_id)
            setTimer(600);
        } catch (error) {
            console.error('Error creating order:', error);
        }
    };

    useEffect(() => {
        if (paymentSuccessful === true) {
            dispatch(clearBasket());
            navigate('/order');
        }
    }, [paymentSuccessful]);

    const handleOpenModal = () => {
        setModalOpen(true);
    };

    useEffect(() => {
        if (orderId) {
            const handlePageLeave = async (event) => {
                event.preventDefault();
                await handleCancelOrderOnPageLeave(orderId, navigate);
                event.returnValue = '';
            };
    
            window.addEventListener('beforeunload', handlePageLeave);
    
            return () => {
                window.removeEventListener('beforeunload', handlePageLeave);
            };
        }
    }, [orderId]);

    return (
        <>
            { showLiqPay && data ? (
                <>
                    <div className="timer-container">
                        <Typography variant="h6" style={{ color: 'red', fontSize: '24px', fontWeight: 'bold' }}>
                            Time Remaining: {Math.floor(timer / 60)}:{timer % 60 < 10 ? '0' : ''}
                            {timer % 60}
                        </Typography>
                        <Button variant="contained" color="secondary" onClick={() => handleOpenModal()}>
                            Cancel Order
                        </Button>
                        {modalOpen && <CloseOrderModal orderID={orderId} open={modalOpen} setModalOpen={setModalOpen} />}
                    </div>
                    <LiqPayComponent
                        data={data}
                        signature={signature}
                        setPaymentSuccessful={setPaymentSuccessful}
                    />
                </>
            ) : (
                <div className="shipping-info-container">
                    <h3 className="shipping-info-title">Shipping Information</h3>
                    <form>
                        <div className="form-container">
                            <Grid container spacing={4}>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        label="Name"
                                        value={buyerName ? buyerName : ''}
                                        error={errorBuyerName}
                                        onChange={handleBuyerNameChange}
                                        required
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        label="Phone"
                                        placeholder="Enter phone number (e.g., +1234567890)"
                                        value={phone ? phone : ''}
                                        error={errorPhone}
                                        inputProps={{ maxLength: 15}}
                                        helperText={!phoneRegex.test(phone) ? 'Invalid phone number format' : ''}
                                        onChange={handlePhoneChange}
                                        required
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        label="Delivery Address"
                                        value={address ? address : ''}
                                        error={errorAddress}
                                        onChange={handleAddressChange}
                                        required
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField
                                        fullWidth
                                        label="Delivery Date"
                                        helperText="Delivery available no earlier than the next day"
                                        type="date"
                                        InputLabelProps={{shrink: true}}
                                        value={selectedDate ? format(selectedDate, 'yyyy-MM-dd') : ''}
                                        onChange={(e) => handleDateChange(new Date(e.target.value))}
                                        required
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField
                                        fullWidth
                                        label="Delivery Time"
                                        helperText="You can choose the time from 9:00 AM to 6:00 PM"
                                        type="time"
                                        InputLabelProps={{shrink: true}}
                                        value={selectedTime ? format(selectedTime, 'HH:mm') : ''}
                                        onChange={(e) => handleTimeChange(e.target.value)}
                                        required
                                    />
                                </Grid>
                            </Grid>
                        </div>
                        <div className="button-container">
                            <Button
                                variant="contained"
                                color="primary"
                                className="pay-button"
                                onClick={() => navigate('/')}
                            >
                                Back to shopping
                            </Button>
                        { !expired && total && (
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handlePayNow}
                                className="pay-button"
                                data-testid="pay-now-button"
                            >
                                {`Pay Now ${total.toFixed(2)} $ (${Math.floor(timer / 60)}:${timer % 60 < 10 ? '0' : ''}${timer % 60})`}
                            </Button>
                        )}
                        </div>
                    </form>
                </div>
            )}
        </>
    );
}

CheckoutFormComponent.propTypes = {
    basketItems: PropTypes.array.isRequired,
    buyerName: PropTypes.string,
    phone: PropTypes.string,
    address: PropTypes.string,
    total: PropTypes.number.isRequired,
    signature: PropTypes.string,
    data: PropTypes.string,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer.basketItems,
    buyerName: state.checkoutReducer.buyerName,
    phone: state.checkoutReducer.phone,
    address: state.checkoutReducer.address,
});

export default connect(mapStateToProps)(CheckoutFormComponent);
