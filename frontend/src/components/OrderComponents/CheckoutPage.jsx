import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Container } from '@mui/material';
import { productReservation } from '@services/api';
import OrderTableComponent from '@components/OrderComponents/OrderTableComponent';
import CheckoutFormComponent from '@components/OrderComponents/CheckoutFormComponent';
import '@components/OrderComponents/OrderPage.css';

function CheckoutPage() {
    const basketItems = useSelector(state => state.basketReducer.basketItems);
    const [orderData, setOrderData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const data = {
                total: basketItems.reduce((sum, item) => sum + item.price * item.amount, 0),
                basketItems: basketItems,
            }
            try {
                const orderData = await productReservation(data);
                setOrderData(orderData);
            } catch (error) {
                console.error('Error fetching order data:', error);
            }
        }
        fetchData();
    }, []);

    return (
        <Container>
        {(orderData.length !== 0) && (
            <div>
                <h2 className="order-heading">Order Details</h2>
                <OrderTableComponent orderData={orderData.basketItems} total={orderData.total} />
                <CheckoutFormComponent total={orderData.total} />
            </div>
        )}
        </Container>
    );
}

CheckoutPage.propTypes = {
    basketItems: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer.basketItems,
});

export default connect(mapStateToProps)(CheckoutPage);
