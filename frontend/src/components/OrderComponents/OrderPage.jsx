import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Container } from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import { fetchOrderData } from '@services/api';
import OrderTableComponent from '@components/OrderComponents/OrderTableComponent';
import ShippingInfoComponent from '@components/OrderComponents/ShippingInfoComponent';
import Loader from '@components/CommonComponents/loader';
import '@components/OrderComponents/OrderPage.css';

function OrderPage() {
    const navigate = useNavigate();
    const { orderID } = useParams();

    const { data, error, isLoading } = useQuery({
        queryKey: ['orderData', orderID],
        queryFn: () => fetchOrderData(orderID),
    });

    if (isLoading) {
        return <Loader />;
    }

    if (error) {
        return <div>Error fetching order data: {error.message}</div>;
    }

    const {
        order_detail_items: orderData = [],
        buyer_name: buyerName,
        phone,
        delivery_address: address,
        delivery_date: date,
        status,
    } = data.data;

    const total = orderData.reduce((sum, item) => sum + item.product__price * item.amount, 0);

    let orderStatus = '';
    let styleStatus = '';

    // Set the order status based on the response
    switch (status) {
        case 'completed':
            orderStatus = 'Completed';
            styleStatus = 'completed';
            break;
        case 'paid':
            orderStatus = 'Paid';
            styleStatus = 'paid';
            break;
        case 'canceled':
            orderStatus = 'Canceled';
            styleStatus = 'canceled';
            break;
        default:
            orderStatus = 'Unknown';
            styleStatus = 'unknown';
            break;
    }

    return (
        <Container>
            <h2 className="order-heading">Order #{orderID.slice(0, 6)}</h2>
            <div className={`order-status ${styleStatus}`}>
                <span>{orderStatus}</span>
                <OrderTableComponent orderData={orderData} total={total} />
            </div>
            <ShippingInfoComponent
                buyerName={buyerName}
                phone={phone}
                address={address}
                date={date}
                navigate={navigate}
            />
        </Container>
    );
}

export default OrderPage;
