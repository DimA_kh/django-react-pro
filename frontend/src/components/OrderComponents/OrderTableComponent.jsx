import React from 'react';
import PropTypes from 'prop-types';
import { Paper, TableContainer, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';

function OrderTableComponent({ orderData, total }) {
    return (
        <div className="order-details-table">
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}>Title</TableCell>
                            <TableCell align="center" style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}>Amount</TableCell>
                            <TableCell align="center" style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}>Price</TableCell>
                            <TableCell align="center" style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}>Sum</TableCell>
                            <TableCell style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {orderData.map((item, index) => (
                            <TableRow key={index}>
                                <TableCell align="center">{item.title}</TableCell>
                                <TableCell align="center">{item.amount}</TableCell>
                                <TableCell align="center">{item.price} $</TableCell>
                                <TableCell align="center">{(item.price * item.amount).toFixed(2)} $</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        ))}
                        <TableRow style={{ backgroundColor: "green" }}>
                            <TableCell style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }} align="center">Total</TableCell>
                            <TableCell style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}></TableCell>
                            <TableCell style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}></TableCell>
                            <TableCell style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }} align="center">{total.toFixed(2)} $</TableCell>
                            <TableCell style={{ fontWeight: 'bold', backgroundColor: "#c8e6c9" }}></TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

OrderTableComponent.propTypes = {
    orderData: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
};

export default OrderTableComponent;
