import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import UserOrders from '@components/OrderComponents/UserOrders';
import * as api from '@services/api';

describe('UserOrders', () => {
    it('renders loader before rendering UserOrders correctly', () => {
        const { getByTestId } = render(
            <MemoryRouter>
                <UserOrders />
            </MemoryRouter>
        );
        const loader = getByTestId('test-loader');
        expect(loader).toBeInTheDocument();
    });

    it('renders UserOrders correctly', async () => {
        const mockOrders = [
            {
                id: 1,
                payment_at: new Date('2022-02-15'),
                phone: '1234567890',
                delivery_address: 'Some address',
                delivery_date: '2022-02-20',
                delivery_time: '10:00',
                total: 50,
                status: 'paid'
            },
            {
                id: 2,
                payment_at: new Date('2022-02-16'),
                phone: '0987654321',
                delivery_address: 'Another address',
                delivery_date: '2022-02-21',
                delivery_time: '12:00',
                total: 75,
                status: 'completed'
            }
        ];
        jest.spyOn(api, 'fetchUserOrders').mockImplementation(() => Promise.resolve(mockOrders));
        const { findByText } = render(
            <MemoryRouter>
                <UserOrders />
            </MemoryRouter>
        );
        await waitFor(() => {
            expect(api.fetchUserOrders).toHaveBeenCalled();
        });

        expect(await findByText('Paid')).toBeInTheDocument();
        expect(await findByText('Completed')).toBeInTheDocument();
        expect(await findByText('Canceled')).toBeInTheDocument();
        expect(await findByText('Order Payment Date')).toBeInTheDocument();
        expect(await findByText('Phone')).toBeInTheDocument();
        expect(await findByText('Delivery Address')).toBeInTheDocument();
        expect(await findByText('Delivery Date')).toBeInTheDocument();
        expect(await findByText('Delivery Time')).toBeInTheDocument();
        expect(await findByText('To Pay')).toBeInTheDocument();
        expect(await findByText('Paid')).toBeInTheDocument();
        expect(await findByText('Actions')).toBeInTheDocument();
    });
});
