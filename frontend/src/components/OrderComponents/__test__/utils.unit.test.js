import { handleCancelOrderOnPageLeave } from '@components/OrderComponents/utils';
import { cancelOrder } from '@services/api';

jest.mock('@services/api');

describe('handleCancelOrderOnPageLeave', () => {
    it('cancels order and navigates to home page', async () => {
        const mockOrderId = '123';
        const mockNavigate = jest.fn();
        await handleCancelOrderOnPageLeave(mockOrderId, mockNavigate);

        expect(cancelOrder).toHaveBeenCalledWith({ orderId: mockOrderId });
        expect(mockNavigate).toHaveBeenCalledWith('/');
    });

    it('handles error when cancelling order', async () => {
        const mockOrderId = '123';
        const mockNavigate = jest.fn();
        const consoleErrorMock = jest.spyOn(console, 'error');
        cancelOrder.mockRejectedValueOnce('Cancellation error');
        await handleCancelOrderOnPageLeave(mockOrderId, mockNavigate);

        expect(cancelOrder).toHaveBeenCalledWith({ orderId: mockOrderId });
        expect(consoleErrorMock).toHaveBeenCalledWith('Error cancelling order:', 'Cancellation error');
    });
});
