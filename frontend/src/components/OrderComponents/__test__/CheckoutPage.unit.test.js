import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import CheckoutPage from '@components/OrderComponents/CheckoutPage';
import { productReservation } from '@services/api';

const mockStore = configureMockStore([]);

jest.mock('@services/api', () => ({
    productReservation: jest.fn(),
}));

jest.mock('@components/OrderComponents/OrderTableComponent', () => {
    return jest.fn(() => <div data-testid="mock-order-table">Mock Order Table Component</div>);
});

jest.mock('@components/OrderComponents/CheckoutFormComponent', () => {
    return jest.fn(() => <div data-testid="mock-checkout-form">Mock Checkout Form Component</div>);
});

describe('CheckoutPage', () => {
    let store;

    beforeEach(() => {
        const initialState = {
            basketReducer: {
                basketItems: [
                    { id: 1, name: 'Product 1', price: 10, amount: 1 },
                    { id: 2, name: 'Product 2', price: 20, amount: 2 },
                ],
            },
            checkoutReducer: {
                buyerName: 'Bob',
                phone: '+1234567890',
                address: 'some address',
            }
        };
        store = mockStore(initialState);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('fetches order data and renders components', async () => {
        const mockOrderData = {
            total: 50,
            basketItems: [
                { id: 1, name: 'Product 1', price: 10, amount: 1 },
                { id: 2, name: 'Product 2', price: 20, amount: 2 },
            ],
        };
        productReservation.mockResolvedValueOnce(mockOrderData);
        const { getByTestId } = render(
            <Provider store={store}>
                <CheckoutPage />
            </Provider>
        );

        await waitFor(() => {
            expect(productReservation).toHaveBeenCalled();
            expect(getByTestId('mock-order-table')).toBeInTheDocument();
            expect(getByTestId('mock-checkout-form')).toBeInTheDocument();
        });
    });
});
