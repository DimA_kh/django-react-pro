import React from 'react';
import { render, waitFor, act } from '@testing-library/react';
import LiqPayComponent from '@components/OrderComponents/LiqPayComponent';

jest.mock('@components/OrderComponents/__test__/__mock__/mockLiqPayScript');

describe('LiqPayComponent', () => {
    it('creates LiqPay script and sets up event handlers', async () => {
        const mockData = 'mockData';
        const mockSignature = 'mockSignature';
        const mockSetPaymentSuccessful = jest.fn();
        render(<LiqPayComponent data={mockData} signature={mockSignature} setPaymentSuccessful={mockSetPaymentSuccessful} />);

        await waitFor(() => {
            expect(document.body.querySelector('script[src="//static.liqpay.ua/libjs/checkout.js"]')).toBeInTheDocument();
            expect(window.LiqPayCheckoutCallback).toBeDefined();
        });
    });

    it('removes LiqPay script when unmounted', async () => {
        const mockData = 'mockData';
        const mockSignature = 'mockSignature';
        const mockSetPaymentSuccessful = jest.fn();

        const { unmount } = render(<LiqPayComponent data={mockData} signature={mockSignature} setPaymentSuccessful={mockSetPaymentSuccessful} />);

        await waitFor(() => {
            expect(document.body.querySelector('script[src="//static.liqpay.ua/libjs/checkout.js"]')).toBeInTheDocument();
            expect(window.LiqPayCheckoutCallback).toBeDefined();
        });

        act(() => {
            unmount();
        });

        await waitFor(() => {
            expect(document.body.querySelector('script[src="//static.liqpay.ua/libjs/checkout.js"]')).not.toBeInTheDocument();
            expect(window.LiqPayCheckoutCallback).toBeUndefined();
        });
    });
});
