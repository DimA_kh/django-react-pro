import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import CheckoutFormComponent from '@components/OrderComponents/CheckoutFormComponent';

const mockStore = configureMockStore();

describe('CheckoutFormComponent', () => {
    let store;

    beforeEach(() => {
        store = mockStore({ 
            basketReducer: { basketItems: [] },
            checkoutReducer: {
                buyerName: 'Bob-3333333',
                phone: '',
                address: '',
            }
        });
    });

    it('renders without crashing', () => {
        const mockProps = {
            total: 200,
            buyerName: 'Bob',
            phone: '+1234567890',
            address: 'some address',
        };
        render(
            <Router>
                <Provider store={store}>
                    <CheckoutFormComponent {...mockProps} />
                </Provider>
            </Router>
        );

        expect(screen.getByText('Back to shopping')).toBeInTheDocument();
        expect(screen.getByText('Delivery Date')).toBeInTheDocument();
        expect(screen.getByText('Delivery available no earlier than the next day')).toBeInTheDocument();
        expect(screen.getByText('Delivery Time')).toBeInTheDocument();
        expect(screen.getByText('You can choose the time from 9:00 AM to 6:00 PM')).toBeInTheDocument();
    });

    it('updates buyerName on change', () => {
        const mockProps = {
            total: 200,
            buyerName: 'Bob',
            phone: '+1234567890',
            address: 'some address',
        };
        const { getByRole } = render(
            <Router>
                <Provider store={store}>
                    <CheckoutFormComponent {...mockProps} />
                </Provider>
            </Router>
        );

        fireEvent.change(getByRole('textbox', { name: 'Name' }), { target: { value: 'John' } });

        const actions = store.getActions();
        expect(actions).toEqual([{ type: 'SET_BUYER_NAME', payload: 'John' }]);
    });
});
