import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { handleCancelOrderOnPageLeave } from '@components/OrderComponents/utils';
import CloseOrderModal from '@components/OrderComponents/ConfirmCloseOrder';

jest.mock('@services/api');
const mockSetModalOpen = jest.fn();

jest.mock('@components/OrderComponents/utils', () => ({
    handleCancelOrderOnPageLeave: jest.fn(),
}));

describe('CloseOrderModal', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('renders correctly when open is true', () => {
        const { container } = render(
            <MemoryRouter>
                <CloseOrderModal orderID="1" open={true} setModalOpen={mockSetModalOpen} />
            </MemoryRouter>
        );

        expect(container).toMatchSnapshot();
    });

    it('calls handleConfirmClose when "Yes" button is clicked', async () => {
        const { getByText } = render(
            <MemoryRouter>
                <CloseOrderModal orderID="1" open={true} setModalOpen={mockSetModalOpen} />
            </MemoryRouter>
        );
        fireEvent.click(getByText('Yes'));

        await waitFor(() => {
            expect(mockSetModalOpen).toHaveBeenCalledWith(false);
            expect(handleCancelOrderOnPageLeave).toHaveBeenCalledWith('1', expect.any(Function));
        });
    });

    it('calls setModalOpen with false when "No" button is clicked', () => {
        const { getByText } = render(
            <MemoryRouter>
                <CloseOrderModal orderID="1" open={true} setModalOpen={mockSetModalOpen} />
            </MemoryRouter>
        );
        fireEvent.click(getByText('No'));

        expect(mockSetModalOpen).toHaveBeenCalledWith(false);
    });
});
