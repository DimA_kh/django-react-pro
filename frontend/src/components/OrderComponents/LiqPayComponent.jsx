import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

const LiqPayComponent = ({ data, signature, setPaymentSuccessful }) => {
    useEffect(() => {
        window.LiqPayCheckoutCallback = function() {
            LiqPayCheckout.init({
                data: data,
                signature: signature,
                embedTo: "#liqpay_checkout",
                mode: "embed" // embed || popup
            }).on("liqpay.callback", function(data){
                if (data.status != "error") {
                    setPaymentSuccessful(true);
                }
                console.warn("callback: ", data);
            }).on("liqpay.ready", function(data){
                console.warn("ready: ", data);
            }).on("liqpay.close", function(data){
                console.warn("close: ", data);
            });
        };

        const script = document.createElement('script');
        script.src = '//static.liqpay.ua/libjs/checkout.js';
        script.async = true;
        document.body.appendChild(script);

        return () => {
            document.body.removeChild(script);
            delete window.LiqPayCheckoutCallback;
        };
    }, [setPaymentSuccessful]);

    return (
        <div id="liqpay_checkout"></div>
    );
};

LiqPayComponent.propTypes = {
    data: PropTypes.string.isRequired,
    signature: PropTypes.string.isRequired,
    setPaymentSuccessful: PropTypes.func.isRequired,

};

export default LiqPayComponent;
