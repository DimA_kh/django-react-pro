import { cancelOrder } from '@services/api';

export const handleCancelOrderOnPageLeave = async (orderId, navigate) => {
    try {
        await cancelOrder({'orderId': orderId});
        navigate('/');
    } catch (error) {
        console.error('Error cancelling order:', error);
    }
};
