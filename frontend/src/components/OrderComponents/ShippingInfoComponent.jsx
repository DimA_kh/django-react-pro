import React from 'react';
import PropTypes from 'prop-types';
import { TextField, Button, Grid } from '@mui/material';
import { format } from 'date-fns';

function ShippingInfoComponent({ buyerName, phone, address, date, navigate }) {

    return (
        <div className="shipping-info-container">
            <h3 className="shipping-info-title">Shipping Information</h3>
            <form>
                <div className="form-container">
                    <Grid container spacing={4}>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Name"
                                value={buyerName}
                                InputProps={{ readOnly: true }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Phone"
                                value={phone}
                                InputProps={{ readOnly: true }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Delivery Address"
                                value={address}
                                InputProps={{ readOnly: true }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                label="Delivery Date"
                                helperText="Delivery available no earlier than the next day"
                                type="date"
                                InputLabelProps={{ shrink: true }}
                                value={format(new Date(date), 'yyyy-MM-dd')}
                                InputProps={{ readOnly: true }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                label="Delivery Time"
                                helperText="You can choose the time from 9:00 AM to 6:00 PM"
                                type="time"
                                InputLabelProps={{ shrink: true }}
                                value={format(new Date(date), 'HH:mm')}
                                InputProps={{ readOnly: true }}
                            />
                        </Grid>
                    </Grid>
                </div>
                <div className="button-container">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => navigate('/order')}
                        className="pay-button"
                    >
                        Back To Order Page
                    </Button>
                </div>
            </form>
        </div>
    );
}

ShippingInfoComponent.propTypes = {
    buyerName: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    navigate: PropTypes.func.isRequired,
};

export default ShippingInfoComponent;