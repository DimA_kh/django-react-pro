import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { styled } from '@mui/system';
import {
    Button,
    Paper,
    Typography,
    Tab,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Tabs,
} from '@mui/material';
import authHeader from '@services/auth.header';
import { fetchUserOrders } from '@services/api';
import Loader from '@components/CommonComponents/loader'

const NoOrdersText = styled(Typography)({
    textAlign: 'center',
    fontSize: '20px',
    margin: '50px auto',
});

const TabOrder = styled('div')(({ theme }) => ({
    flex: 1,
    textTransform: 'none',
    fontWeight: 'bold',
    fontSize: '16px',
    color: theme.palette.text.primary,
    '&.Mui-selected': {
        color: 'rgb(65, 114, 90)',
    },
}));

function UserOrders() {
    // 0: "Paid", 1: "Completed", 2: "Canceled"
    const [selectedTabIndex, setTabValue] = useState(0);
    const navigate = useNavigate();
    const [orders, setOrders] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchUserOrdersFromServer = async () => {
            try {
                authHeader();
                const orders = await fetchUserOrders();
                setOrders(orders);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching user orders:', error);
            }
        };
        fetchUserOrdersFromServer();
    }, []);

    const getOrderAndNavigate = async (orderID) => {
        navigate(`/order/${orderID}`);
    };

    const filterOrders = () => {
        switch (selectedTabIndex) {
            case 0:
                return orders?.filter(order => order?.status === 'paid');
            case 1:
                return orders?.filter(order => order?.status === 'completed');
            case 2:
                return orders?.filter(order => order?.status === 'canceled');
            default:
                return orders;
        }
    };

    return (
        <main className="container">
            {loading ? (
                <Loader />
            ) : orders.length === 0 ? (
                <NoOrdersText>No orders yet.</NoOrdersText>
            ) : (
            <div>
                <Tabs
                    value={selectedTabIndex}
                    onChange={(e, newValue) => setTabValue(newValue)}
                    centered
                    sx={{ display: 'flex', width: '100%' }}
                >
                    <Tab
                        label="Paid"
                        sx={{ flex: 1, textTransform: 'none', fontWeight: 'bold', fontSize: '16px', color: theme => theme.palette.text.primary, '&.Mui-selected': { color: 'rgb(65, 114, 90)' } }}
                    />
                    <Tab
                        label="Completed"
                        sx={{ flex: 1, textTransform: 'none', fontWeight: 'bold', fontSize: '16px', color: theme => theme.palette.text.primary, '&.Mui-selected': { color: 'rgb(65, 114, 90)' } }}
                    />
                    <Tab
                        label="Canceled"
                        sx={{ flex: 1, textTransform: 'none', fontWeight: 'bold', fontSize: '16px', color: theme => theme.palette.text.primary, '&.Mui-selected': { color: 'rgb(65, 114, 90)' } }}
                    />
                </Tabs>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell>Order Payment Date</TableCell>
                            <TableCell>Phone</TableCell>
                            <TableCell>Delivery Address</TableCell>
                            <TableCell>Delivery Date</TableCell>
                            <TableCell>Delivery Time</TableCell>
                            <TableCell>
                                {selectedTabIndex === 0 ? 'To Pay' : 'Paid'}
                            </TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {filterOrders().map((order, index) => (
                            <TableRow key={index}>
                                <TableCell>{index + 1}</TableCell>
                                <TableCell>{new Date(order.payment_at).toISOString().split('T')[0]}</TableCell>
                                <TableCell>{order.phone}</TableCell>
                                <TableCell>{order.delivery_address}</TableCell>
                                <TableCell>{order.delivery_date}</TableCell>
                                <TableCell>{order.delivery_time}</TableCell>
                                <TableCell>{order.total} $</TableCell>
                                <TableCell>
                                    <Button onClick={() => getOrderAndNavigate(order.id)} color="primary">
                                        Go to order
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <Paper className={TabOrder.contentPaper}>
                    {selectedTabIndex === 0 && (
                        <Typography className={TabOrder.contentText}>
                            * Orders in this tab have been successfully paid and will be delivered
                            to the specified address on the chosen delivery date and time.
                        </Typography>
                    )}
                    {selectedTabIndex === 1 && (
                        <Typography className={TabOrder.contentText}>
                            * Here you can find information about orders that have been successfully completed.
                            You can provide feedback about your experience, including what you liked or disliked,
                            using the feedback form. Your input will help us enhance our service.
                        </Typography>
                    )}
                    {selectedTabIndex === 2 && (
                        <Typography className={TabOrder.contentText}>
                            * Canceled orders.
                        </Typography>
                    )}
                </Paper>
            </div>
            )}
        </main>
    );
}

export default UserOrders;
