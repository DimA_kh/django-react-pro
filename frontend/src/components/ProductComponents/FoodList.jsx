import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row } from 'reactstrap';
import FoodCard from '@components/ProductComponents/FoodCard';

function FoodList(props) {

    return (
        <Container>
            <Row>
                {props.allItems.map((item, index) => (
                    <FoodCard
                        {...item}
                        key={index}
                        onHandleAppend={props.onHandleAppend}
                        onToggleShow={props.onToggleShow}
                        showBasket={props.showBasket}
                    />
                ))}
            </Row>
        </Container>
    );
}

FoodList.propTypes = {
    allItems: PropTypes.array.isRequired,
    onHandleAppend: PropTypes.func.isRequired,
};

export default FoodList;
