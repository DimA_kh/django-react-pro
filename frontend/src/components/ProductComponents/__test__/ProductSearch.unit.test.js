import React from 'react';
import { Provider } from 'react-redux';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import ProductSearch from '@components/ProductComponents/ProductSearch';

jest.mock('@services/api', () => ({
    searchProducts: jest.fn(() => Promise.resolve([]))
}));

const mockStore = configureStore([]);
const queryClient = new QueryClient();

jest.mock('@components/ProductComponents/SearchResults', () => () => (
    <div data-testid="mock-search-results">Mocked Search Results</div>
));

jest.mock('@services/api', () => ({
    searchProducts: jest.fn().mockResolvedValue([
        { id: 1, title: 'Test Product' }
    ]),
}));

describe('ProductSearch Component', () => {
    let store;

    beforeEach(() => {
        store = mockStore({
            basketReducer: { basketItems: [] }
        });
    });

    it('renders correctly', () => {
        render(
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    <ProductSearch />
                </QueryClientProvider>
            </Provider>
        );
        expect(screen.getByPlaceholderText('Search...')).toBeInTheDocument();
    });

    it('updates query on input change', () => {
        render(
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    <ProductSearch />
                </QueryClientProvider>
            </Provider>
        );
        const input = screen.getByPlaceholderText('Search...');
        fireEvent.change(input, { target: { value: 'test' } });

        expect(input.value).toBe('test');
    });

    it('shows results when query length is 2 or more', async () => {
        render(
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    <ProductSearch />
                </QueryClientProvider>
            </Provider>
        );
        const input = screen.getByPlaceholderText('Search...');
        fireEvent.change(input, { target: { value: 'te' } });
        await waitFor(() => expect(screen.getByTestId('mock-search-results')).toBeInTheDocument());

        expect(await screen.findByTestId('mock-search-results')).toBeInTheDocument();
    });
});
