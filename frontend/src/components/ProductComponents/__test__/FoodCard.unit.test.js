import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { render, screen, cleanup } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import FoodCard from '@components/ProductComponents/FoodCard';
import AuthService from '@services/auth.service';

jest.mock('@services/auth.service');
const mockStore = configureMockStore();

const renderFoodCard = (props, basketItems = []) => {
    const store = mockStore({ basketReducer: { basketItems } });
    return render(
        <Router>
            <Provider store={store}>
                <FoodCard {...props} />
            </Provider>
        </Router>
    );
};

describe('FoodCard', () => {
    const mockProps = {
        id: 1,
        title: 'Test Food',
        image: 'test-image.jpg',
        price: 10,
        quantity: 5,
        description: 'Test description',
        showBasket: true,
    };

    afterEach(() => {
        cleanup();
        jest.resetAllMocks();
    });

    it('renders without crashing', () => {
        const { container } = renderFoodCard(mockProps);
        expect(container).toMatchSnapshot();
    });

    it('shows "Add to basket" button when user is logged in and item is not in basket', () => {
        AuthService.getCurrentUser.mockReturnValue({ id: 1, username: 'wolf' });
        renderFoodCard(mockProps, []);

        expect(screen.getByText('Add to basket')).toBeInTheDocument();
    });

    it('shows "Show in basket" button when item is in basket', () => {
        AuthService.getCurrentUser.mockReturnValue({ id: 1, username: 'wolf' });
        renderFoodCard(mockProps, [{ id: 1 }]);

        expect(screen.getByText('Show in basket')).toBeInTheDocument();
    });

    it('shows "Add to basket" button when user is not logged in', () => {
        AuthService.getCurrentUser.mockReturnValue(null);

        renderFoodCard(mockProps, []);

        expect(AuthService.getCurrentUser()).toBeNull();
        expect(screen.getByText('Add to basket')).toBeInTheDocument();
    });
});
