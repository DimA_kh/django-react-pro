import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import SearchResults from '@components/ProductComponents/SearchResults';

// Create a mock reducer and store
const mockReducer = (state = {}, action) => state;
const store = createStore(mockReducer);

const mockHandleAddToBasket = jest.fn();
const mockHandleCloseResults = jest.fn();
const mockIsInBasket = jest.fn();

describe('SearchResults Component', () => {
    it('renders loading spinner when loading', () => {
        render(
            <Provider store={store}>
                <SearchResults
                    results={[]}
                    isLoading={true}
                    isError={false}
                    isInBasket={mockIsInBasket}
                    handleAddToBasket={mockHandleAddToBasket}
                    handleCloseResults={mockHandleCloseResults}
                />
            </Provider>
        );

        expect(screen.getByRole('status')).toBeInTheDocument();
    });

    it('renders error message when there is an error', () => {
        render(
            <Provider store={store}>
                <SearchResults
                    results={[]}
                    isLoading={false}
                    isError={true}
                    isInBasket={mockIsInBasket}
                    handleAddToBasket={mockHandleAddToBasket}
                    handleCloseResults={mockHandleCloseResults}
                />
            </Provider>
        );

        expect(screen.getByText('Error occurred while searching.')).toBeInTheDocument();
    });

    it('renders no results message when results are empty', () => {
        render(
            <Provider store={store}>
                <SearchResults
                    results={[]}
                    isLoading={false}
                    isError={false}
                    isInBasket={mockIsInBasket}
                    handleAddToBasket={mockHandleAddToBasket}
                    handleCloseResults={mockHandleCloseResults}
                />
            </Provider>
        );

        expect(screen.getByText('Not found.')).toBeInTheDocument();
    });

    it('renders search results correctly', () => {
        const mockResults = [
            { id: 1, title: 'Product 1', price: 10, stock: 5, image: 'image1.jpg' },
            { id: 2, title: 'Product 2', price: 20, stock: 2, image: 'image2.jpg' },
        ];
        render(
            <Provider store={store}>
                <SearchResults
                    results={mockResults}
                    isLoading={false}
                    isError={false}
                    isInBasket={mockIsInBasket}
                    handleAddToBasket={mockHandleAddToBasket}
                    handleCloseResults={mockHandleCloseResults}
                />
            </Provider>
        );

        mockResults.forEach(product => {
            expect(screen.getByText(product.title)).toBeInTheDocument();
            expect(screen.getByText(`Price: ${product.price}$`)).toBeInTheDocument();
        });
    });

    it('calls handleCloseResults when Close Results is clicked', () => {
        render(
            <Provider store={store}>
                <SearchResults
                    results={[]}
                    isLoading={false}
                    isError={false}
                    isInBasket={mockIsInBasket}
                    handleAddToBasket={mockHandleAddToBasket}
                    handleCloseResults={mockHandleCloseResults}
                />
            </Provider>
        );

        fireEvent.click(screen.getByText('Close Results'));
        expect(mockHandleCloseResults).toHaveBeenCalled();
    });
});
