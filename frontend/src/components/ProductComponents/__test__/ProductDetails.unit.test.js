import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import ProductDetails from '@components/ProductComponents/ProductDetails';
import { fetchItem } from '@services/api';
import { QueryClient, QueryClientProvider, useQuery } from '@tanstack/react-query';

const mockStore = configureStore([]);
jest.mock('@services/api');
jest.mock('@tanstack/react-query', () => ({
    ...jest.requireActual('@tanstack/react-query'),
    useQuery: jest.fn(),
}));

describe('ProductDetails', () => {
    let portalRoot;
    const queryClient = new QueryClient();
    const store = mockStore({
        basketReducer: { basketItems: [] },
    });

    beforeEach(() => {
        // Create a root portal for rendering via ReactDOM.createPortal
        portalRoot = document.createElement('div');
        portalRoot.setAttribute('id', 'portal-root');
        document.body.appendChild(portalRoot);
    });

    afterEach(() => {
        // Clear the root portal after each test
        document.body.removeChild(portalRoot);
        portalRoot = null;
    });

    it('should render product details correctly', async () => {
        // Mock the fetchItem API request
        const mockProduct = {
            id: 1,
            title: 'Test Product',
            image: 'test.jpg',
            price: 10,
            quantity: 5,
            description: 'Test description',
        };
        fetchItem.mockResolvedValue(mockProduct);
        // Mocking useQuery to test a successful query
        useQuery.mockReturnValue({
            data: mockProduct,
            error: null,
            isLoading: false,
        });

        render(
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    <Router>
                        <ProductDetails id={1} />
                    </Router>
                </QueryClientProvider>
            </Provider>,
            { container: document.getElementById('portal-root') }
        );

        await waitFor(() => {
            expect(screen.getByAltText('food')).toBeInTheDocument();
        });

        expect(screen.getByText(`Price: ${mockProduct.price}$`)).toBeInTheDocument();
        expect(screen.getByText(`Remaining stock: ${mockProduct.quantity}`)).toBeInTheDocument();
        expect(screen.getByText(mockProduct.description)).toBeInTheDocument();
        expect(screen.getByTestId('test-product-details')).toMatchSnapshot();
    });

    it('should display an error message when fetching fails', async () => {
        // Mocking the fetchItem API request error
        fetchItem.mockRejectedValue(new Error('Failed to fetch product'));
        // Mocking useQuery to test errors
        useQuery.mockReturnValue({
            data: null,
            error: new Error('Failed to fetch product'),
            isLoading: false,
        });

        render(
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    <Router>
                        <ProductDetails id={1} />
                    </Router>
                </QueryClientProvider>
            </Provider>,
            { container: document.getElementById('portal-root') }
        );

        // Check that the component displays an error after retrying the request
        await waitFor(() => {
            expect(screen.queryByTestId('test-loader')).not.toBeInTheDocument();
            expect(screen.getByTestId('test-error')).toBeInTheDocument();
        });
        expect(screen.getByText('Error fetching product data: Failed to fetch product')).toBeInTheDocument();
    });
});
