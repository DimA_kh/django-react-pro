import React from 'react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import FoodList from '@components/ProductComponents/FoodList';

const mockStore = configureStore([]);

describe('FoodList', () => {
    const initialState = { 
        basketReducer: {
            basketItems: [
                { id: 1, title: 'Item 1', price: 10, quantity: 20, image: 'image1.jpg' },
            ],
        }
    };
    const store = mockStore(initialState);
    const mockAllItems = [
        { id: 1, title: 'Item 1', price: 10, quantity: 20, image: 'image1.jpg' },
        { id: 2, title: 'Item 2', price: 15, quantity: 30, image: 'image2.jpg' },
    ];
    const onHandleAppend = jest.fn();

    it('renders correctly', () => {
        const { getByTestId } = render(
            <MemoryRouter>
                <Provider store={store}>
                    <FoodList
                        allItems={mockAllItems}
                        onHandleAppend={onHandleAppend}
                    />
                </Provider>
            </MemoryRouter>
        );

        mockAllItems.forEach(item => {
            const foodCard = getByTestId(`test-product-${item.id}`);
            expect(foodCard).toBeInTheDocument();
        });
    });
});
