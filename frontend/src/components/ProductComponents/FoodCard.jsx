import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { 
    Button,
    Card,
    CardContent,
    CardMedia,
    Typography,
} from '@mui/material';
import ProductDetails from '@components/ProductComponents/ProductDetails';
import '@components/ProductComponents/FoodCard.css';
import AuthService from '@services/auth.service';

const currentUser = AuthService.getCurrentUser();

function FoodCard(props) {
    const [showModal, setShowModal] = useState(false);
    const { id, title, image, price, quantity } = props;
    const item = { id: id, title: title, price: price, quantity: quantity, image: image };
    const isItemInBasket = props.basketItems && props.basketItems.map(getId).includes(id);

    function getId(item) {
        return item.id;
    }

    const toggleModal = () => {
        setShowModal(!showModal);
    };

    const handleModalKeyPress = (event) => {
        if (event.key === 'Enter') {
            toggleModal();
        }
    };

    return (
        <Card
            id={`product-${id}`} 
            data-testid={`test-product-${id}`} 
            className={`food-card ${isItemInBasket ? "main-color" : ""}`}
            sx={{
                width: '17rem',
                textAlign: 'center',
                position: 'relative',
                margin: '20px',
            }}
        >
            <div
                id="food-img"
                tabIndex={0}
                role="button"
                onClick={toggleModal}
                onKeyDown={handleModalKeyPress}
            >
                <CardMedia
                    component="img"
                    image={image}
                    height="210"
                    width="255"
                    alt="food"
                    sx={{ borderRadius: '5px', alignSelf: 'flex-start', objectFit: 'cover' }}
                />
            </div>
            <CardContent>
                <Typography variant="h5" component="h2">
                    {title}
                    {isItemInBasket && (
                        <div className="item-in-basket">
                            In Basket
                        </div>
                    )}
                </Typography>
                <Typography variant="subtitle1" color="text.secondary" gutterBottom>
                    Price: {price}$
                </Typography>
                <Typography variant="subtitle1" color="text.secondary" gutterBottom>
                    Remaining stock: {quantity}
                </Typography>
                {quantity > 0 ? (
                    currentUser ? (
                        isItemInBasket ? (
                        <Button onClick={props.onToggleShow}>
                            Show in basket
                        </Button>
                        ) : (
                        <Button
                            variant="contained"
                            color="success"
                            onClick={() => props.onHandleAppend(item, 1)}
                        >
                            Add to basket
                        </Button>
                        )
                    ) : (
                        isItemInBasket ? (
                            <Link to="/login" style={{ textDecoration: 'none' }}>
                                <Button>Show in basket</Button>
                            </Link>
                        ) : (
                            <Link to="/login" style={{ textDecoration: 'none' }}>
                                <Button variant="contained" color="success">Add to basket</Button>
                            </Link>
                        )
                    )
                ) : null}
            </CardContent>
            {showModal && (
                <ProductDetails 
                    id={id}
                    onHandleAppend={props.onHandleAppend}
                    onToggleModal={toggleModal}
                />
            )}
        </Card>
    );
}

FoodCard.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    description: PropTypes.string,
    onHandleAppend: PropTypes.func.isRequired,
    // from redux
    basketItems: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer.basketItems,
});

export default connect(mapStateToProps)(FoodCard);
