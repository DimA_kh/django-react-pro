import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Button, Spinner } from 'reactstrap';
import ProductDetails from '@components/ProductComponents/ProductDetails';
import { addToBasket } from '@data/actions/basketActions';

const SearchResults = memo(({ results, isLoading, isError, isInBasket, handleCloseResults }) => {
    const [showModal, setShowModal] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [showSearchResults, setShowSearchResults] = useState(true);
    const dispatch = useDispatch();
    const searchResultsRef = useRef(null);

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (searchResultsRef.current && 
                !searchResultsRef.current.contains(event.target) && 
                showSearchResults) {
                handleCloseResults();
            }
        };

        document.addEventListener('mousedown', handleClickOutside);
        
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [handleCloseResults, showSearchResults]);

    const toggleModal = (product = null) => {
        setSelectedProduct(product);
        setShowModal(!!product);
        setShowSearchResults(!product);
    };

    const handleAddToBasket = (item) => {
        const newItem = {
            id: item.id,
            title: item.title,
            price: item.price,
            image: item.image,
            quantity: item.stock,
            amount: 1,
        };
        dispatch(addToBasket(newItem));
    };

    return (
        <>
            {showSearchResults && (
                <div className="search-results" ref={searchResultsRef}>
                    {isLoading ? (
                        <div className="loading-spinner">
                            <Spinner color="primary" />
                        </div>
                    ) : isError ? (
                        <div className="error-message">Error occurred while searching.</div>
                    ) : results.length > 0 ? (
                        results.map((result) => (
                            <div key={result.id} className="search-result-item">
                                <img src={result.image} alt={result.title} className="search-result-image" />
                                <div className="search-result-details">
                                    <h5>{result.title}</h5>
                                    <p>Price: {result.price}$</p>
                                    <p>Stock: {result.stock}</p>
                                    <div className="search-result-buttons">
                                        <Button color="success" size="sm" className="mr-2" onClick={() => toggleModal(result)}>
                                            View Details
                                        </Button>
                                        {isInBasket(result.id) ? (
                                            <Button color="secondary" size="sm" disabled>In Basket</Button>
                                        ) : (
                                            <Button
                                                color="success"
                                                size="sm"
                                                onClick={() => handleAddToBasket(result)}
                                            >
                                                Add to Basket
                                            </Button>
                                        )}
                                    </div>
                                </div>
                            </div>
                        ))
                    ) : (
                        <div className="no-results">Not found.</div>
                    )}
                    <div className="close-results">
                        <Button color="outline-secondary" size="sm" onClick={handleCloseResults}>Close Results</Button>
                    </div>
                </div>
            )}

            {showModal && selectedProduct && (
                <ProductDetails
                    id={selectedProduct.id}
                    onHandleAppend={handleAddToBasket}
                    onToggleShow={() => setShowModal(false)}
                    onToggleModal={() => {
                        toggleModal(null);
                        setShowSearchResults(true);
                    }}
                    showBasket={false}
                />
            )}
        </>
    );
});

SearchResults.propTypes = {
    results: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
    isInBasket: PropTypes.func.isRequired,
    handleCloseResults: PropTypes.func.isRequired,
};

export default SearchResults;
