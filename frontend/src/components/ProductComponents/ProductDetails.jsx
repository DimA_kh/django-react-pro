import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { useQuery } from '@tanstack/react-query';
import { Link } from 'react-router-dom';
import { fetchItem } from '@services/api';
import AuthService from '@services/auth.service';
import Loader from '@components/CommonComponents/loader';
import '@components/ProductComponents/ProductDetails.css';

function ProductDetails(props) {
    const { id, basketItems, onToggleModal, onHandleAppend } = props;
    const currentUser = AuthService.getCurrentUser();

    // Using ReactQuery to get product data
    const { data: product, error, isLoading } = useQuery({
        queryKey: ['product', id], // Unique key for caching
        queryFn: () => fetchItem(id), // Data receive function
        staleTime: 10 * 60 * 1000, // The data cache will be valid for 10 minutes
        retry: 1, // Try to execute the request again if an error occurs
    });

    if (isLoading) {
        return <Loader />;
    }

    if (error) {
        return (
            <div data-testid="test-error">
                Error fetching product data: {error.message}
            </div>
        );
    }

    const content = product ? (
        <div data-testid="test-product-details">
            <div className="product-container">
                <div className="product-image">
                    <img alt="food" src={product.image} className="image" />
                </div>
                <div className="product-content">
                    <button className="close-button" onClick={onToggleModal}>×</button>
                    <div className="product-details">
                        <p className="price">Price: {product.price}$</p>
                        <p className="quantity">Remaining stock: {product.quantity}</p>
                        <div>
                            {product.quantity > 0 ? (
                                currentUser ? (
                                    basketItems.some((item) => item.id === product.id) ? (
                                        <button className="button-show-in-basket" disabled>In Basket</button>
                                    ) : (
                                        <button
                                            className="button-add-to-basket"
                                            onClick={() => onHandleAppend(product, 1)}
                                        >
                                            Add to basket
                                        </button>
                                    )
                                ) : (
                                    <Link to="/login" style={{ textDecoration: 'none' }}>
                                        <button className="button-login">
                                            Login to add to basket
                                        </button>
                                    </Link>
                                )
                            ) : (
                                <p className="out-of-stock">Out of stock</p>
                            )}
                        </div>
                    </div>
                    <div className="description">
                        <p>{product?.description}</p>
                    </div>
                </div>
            </div>
        </div>
    ) : (
        <Loader />
    );

    return ReactDOM.createPortal(
        content,
        document.getElementById('portal-root')
    );
}

ProductDetails.propTypes = {
    id: PropTypes.number.isRequired,
    onToggleModal: PropTypes.func.isRequired,
    onHandleAppend: PropTypes.func.isRequired,
    // from Redux
    basketItems: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer.basketItems,
});

export default connect(mapStateToProps)(ProductDetails);
