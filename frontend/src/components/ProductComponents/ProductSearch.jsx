import React, { useState, useRef, useEffect, forwardRef } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Col, Input, Form } from 'reactstrap';
import { searchProducts } from '@services/api';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addToBasket } from '@data/actions/basketActions';
import SearchResults from '@components/ProductComponents/SearchResults';
import '@components/ProductComponents/ProductSearch.css';
import { debounce } from 'lodash';

const ProductSearch = forwardRef(({ basketItems, addToBasket }, ref) => {
    const [query, setQuery] = useState('');
    const [showResults, setShowResults] = useState(false);
    const searchRef = useRef(null);

    const debouncedSetQuery = useRef(
        debounce((query) => setQuery(query), 300)
    ).current;

    const { data: results, isLoading, isError } = useQuery({
        queryKey: ['searchProducts', query],
        queryFn: () => searchProducts(query),
        enabled: query.length >= 2,
        refetchOnWindowFocus: false,
        refetchInterval: 0,
    });

    useEffect(() => {
        if (query.length < 2) {
            setShowResults(false);
        } else {
            setShowResults(true);
        }
    }, [query]);

    const handleSearch = (event) => {
        event.preventDefault();
        setShowResults(true);
    };

    const handleInputChange = (e) => {
        debouncedSetQuery(e.target.value);
    };

    const handleAddToBasket = (product) => {
        const newItem = {
            id: product.id,
            title: product.title,
            price: product.price,
            image: product.image,
            quantity: product.stock,
            amount: 1,
        };
        addToBasket(newItem);
    };

    const isInBasket = (productId) => {
        return basketItems.some(item => item.id === productId);
    };

    const handleCloseResults = () => {
        setShowResults(false);
        setQuery('');
    };

    return (
        <div className="search-container d-none d-lg-flex justify-content-end" ref={searchRef}>
            <Col>
                <Form onSubmit={handleSearch}>
                    <Input
                        type="search"
                        className="mr-3"
                        placeholder="Search..."
                        onChange={handleInputChange}
                    />
                </Form>

                {showResults && (
                    <SearchResults
                        results={results || []}
                        isLoading={isLoading}
                        isError={isError}
                        isInBasket={isInBasket}
                        handleAddToBasket={handleAddToBasket}
                        handleCloseResults={handleCloseResults}
                    />
                )}
            </Col>
        </div>
    );
});

ProductSearch.propTypes = {
    basketItems: PropTypes.array.isRequired,
    addToBasket: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer?.basketItems,
});

const mapDispatchToProps = {
    addToBasket,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductSearch);
