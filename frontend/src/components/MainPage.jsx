/**
 * Component responsible for displaying and managing the content of the application.
 *
 * This component fetches data, displays food items, and manages the shopping basket.
 *
 * @component
 */
import React, { useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@tanstack/react-query';
import PropTypes from 'prop-types';
import FoodList from '@components/ProductComponents/FoodList';
import BasketIcon from '@components/BasketComponents/BasketIcon';
import Basket from '@components/BasketComponents/Basket';
import ShowAlert from '@components/CommonComponents/ShowAlert';
import {
    addToBasket,
    setBasketItems,
} from '@data/actions/basketActions';
import { fetchItems } from '@services/api';

function MainPage() {
    const dispatch = useDispatch();
    const [showBasket, setShowBasket] = useState(false);
    const [showAlert, setShowAlert] = useState(null);
    const basketItems = useSelector(state => state.basketReducer?.basketItems);

    // Using React Query to get product data
    const { data: allItems, error, isLoading } = useQuery({
        queryKey: ['products'],
        queryFn: fetchItems,
    });

    // Handles the addition of an item to the shopping basket.
    const handleAppend = (item, amount = 1) => {
        const newItem = {
            id: item.id,
            title: item.title,
            price: item.price,
            image: item.image,
            quantity: item.quantity,
            amount: amount,
        };
        dispatch(addToBasket(newItem));
        setShowAlert(`${item.title} added to basket`);
    };

    // Toggles the visibility of the shopping basket.
    const toggleShow = () => setShowBasket(!showBasket);

    // Hides the displayed alert message.
    const hideAlert = () => setShowAlert(null);

    if (isLoading) return <div>Loading...</div>;
    if (error) return <div>Error: {error.message}</div>;

    return (
        <main className="container">
            <BasketIcon
                onToggleShow={toggleShow}
            />
            {showAlert && (
                <ShowAlert
                    onShowAlert={showAlert}
                    onHideAlert={hideAlert}
                />
            )}
            <FoodList
                allItems={allItems}
                onHandleAppend={handleAppend}
            />
            {showBasket && (
                <Basket
                    allItems={allItems}
                    basketItems={basketItems}
                    onToggleShow={toggleShow}
                    showBasket={showBasket}
                />
            )}
        </main>
    );
}

MainPage.propTypes = {
    basketItems: PropTypes.array.isRequired,
    setBasketItems: PropTypes.func.isRequired,
    addToBasket: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer?.basketItems,
});

const mapDispatchToProps = {
    setBasketItems,
    addToBasket,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
