import React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, TableCell, TableRow } from '@mui/material';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import {
    handleRemove,
    handleIncrement,
    handleDecrement,
} from '@components/BasketComponents/basketHandlers';

const BasketItemComponent = React.memo(function BasketItemComponent(props) {
    const dispatch = useDispatch();

    return (
        <TableRow style={{backgroundColor: "rgb(122, 155, 139)"}}>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>{props.title}</TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>
                <img alt="food" src={props.image} height="40" width="50" />
            </TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>
            {props.amount > 1 && (
                <Button aria-label="decrement" onClick={() => handleDecrement(dispatch, props.item)}>&lt;</Button>
            )}
            </TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>{props.amount}</TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>
            {props.quantity !== props.amount && (
                <Button aria-label="increment" onClick={() => handleIncrement(dispatch, props.item)}>&gt;</Button>
            )}
            </TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>{props.price} $</TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>{props.quantity}</TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>{props.price * props.amount} $</TableCell>
            <TableCell align="center" style={{ backgroundColor: "#c8e6c9" }}>
                <Button aria-label="delete-btn" color="secondary" onClick={() => handleRemove(dispatch, props.id)}>
                    <DeleteOutlineIcon />
                </Button>
            </TableCell>
        </TableRow>
    );
});

BasketItemComponent.propTypes = {
    item: PropTypes.object.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.number,
    amount: PropTypes.number,
    quantity: PropTypes.number,
};

export default BasketItemComponent;
