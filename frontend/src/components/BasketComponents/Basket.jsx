import React, { useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { 
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions 
} from '@mui/material';
import AuthService from '@services/auth.service';
import BasketTableComponent from '@components/BasketComponents/BasketTableComponent';

function Basket(props) {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const total = props.basketItems.reduce((sum, item) => sum + item.price * item.amount, 0);
    const paperStyle = {
        backgroundColor: "#c8e6c9",
    };

    useEffect(() => {
        dispatch({ type: 'FETCH_DATA' });
    }, [dispatch]);

    const currentUser = AuthService.getCurrentUser();

    return (
        <Dialog open={props.showBasket} onClose={props.onToggleShow} fullWidth maxWidth="md" PaperProps={{style: paperStyle}}>
            <DialogTitle className="red-text text-lighten-1" style={{ fontWeight: 'bold' }} align="center">My basket</DialogTitle>
            <DialogContent>
                {props.basketItems.length ? (
                    <BasketTableComponent basketItems={props.basketItems} total={total}/>
                ) : (
                    <p style={{ textAlign: 'center' }}>Basket is empty</p>
                )}
            </DialogContent>
            <DialogActions>
                <Button color="secondary" onClick={props.onToggleShow}>
                    Close
                </Button>
                { total ? (
                    currentUser ? (
                        <Button color="primary" onClick={() => navigate(`/checkout`)}>
                            Proceed to payment
                        </Button>
                    ) : (
                        <Link to="/login" style={{ textDecoration: 'none' }}>
                            <Button color="primary">
                                Proceed to payment
                            </Button>
                        </Link>
                    )
                ) : null}
            </DialogActions>
        </Dialog>
    );
}

Basket.propTypes = {
    basketItems: PropTypes.array.isRequired,
    showBasket: PropTypes.bool.isRequired,
    onToggleShow: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer.basketItems,
});

export default connect(mapStateToProps)(Basket);
