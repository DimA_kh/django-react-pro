import {
    incrementAmount,
    decrementAmount,
    removeFromBasket,
} from '@data/actions/basketActions';

/**
 * Handles the decrement of the amount of an item in the shopping basket.
 * Removes the item from the basket if the amount reaches 1.
 *
 * @param {Object} item - The item for which the amount is decremented.
 */
export function handleRemove(dispatch, id) {
    dispatch(removeFromBasket(id));
}

/**
 * Handles the increment of the amount of an item in the shopping basket.
 *
 * @param {Object} item - The item for which the amount is incremented.
 */
export function handleIncrement(dispatch, item) {
    if (item.amount < item.quantity) {
        dispatch(incrementAmount(item.id));
    }
}

/**
 * Handles the decrement of the amount of an item in the shopping basket.
 *
 * @param {Object} item - The item for which the amount is decremented.
 */
export function handleDecrement(dispatch, item) {
    if (item.amount > 1) {
        dispatch(decrementAmount(item.id));
    } else {
        dispatch(removeFromBasket(item.id));
    }
}
