import React from 'react';
import PropTypes from 'prop-types';
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper } from '@mui/material';
import BasketItemComponent from '@components/BasketComponents/BasketItemComponent';
import '@components/OrderComponents/OrderPage.css';

function BasketTableComponent({ basketItems, total }) {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} size="small">
                <TableHead>
                    <TableRow style={{ backgroundColor: "#c8e6c9" }}>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Title</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Image</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Decrease</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Amount</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Increase</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Price</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Remaining stock</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>Sum</TableCell>
                        <TableCell style={{ fontWeight: 'bold' }}></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {basketItems.map((item) => (
                        <BasketItemComponent key={item.id} item={item} {...item} />
                    ))}
                    <TableRow style={{ backgroundColor: "#c8e6c9" }}>
                        <TableCell align="right" style={{ fontWeight: 'bold' }} colSpan={7} >Total</TableCell>
                        <TableCell align="center" style={{ fontWeight: 'bold' }}>{total.toFixed(2)} $</TableCell>
                        <TableCell style={{ fontWeight: 'bold' }}></TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
}

BasketTableComponent.propTypes = {
    basketItems: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
};

export default BasketTableComponent;
