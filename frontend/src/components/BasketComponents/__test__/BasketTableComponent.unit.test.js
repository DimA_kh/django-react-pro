import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import BasketTableComponent from '@components/BasketComponents/BasketTableComponent';

const mockStore = configureMockStore();

describe('BasketTableComponent', () => {
    const store = mockStore({
        basketReducer: { basketItems: [] }
    });

    const basketItems = [
        { id: 1, title: 'Item 1', image: 'image1.jpg', price: 10, amount: 2, quantity: 8 },
        { id: 2, title: 'Item 2', image: 'image2.jpg', price: 15, amount: 1, quantity: 5 }
    ];

    it('renders table with correct items and total', () => {
        const total = basketItems.reduce((acc, item) => acc + item.price * item.quantity, 0);
        const { getByText } = render(
            <Provider store={store}>
                <BasketTableComponent basketItems={basketItems} total={total} />
            </Provider>
        );

        expect(getByText('Item 1')).toBeInTheDocument();
        expect(getByText('Item 2')).toBeInTheDocument();
        expect(getByText(`${total.toFixed(2)} $`)).toBeInTheDocument();
    });

    it('renders table with correct number of rows', () => {
        const total = basketItems.reduce((acc, item) => acc + item.price * item.quantity, 0);
        const { getAllByRole } = render(
            <Provider store={store}>
                <BasketTableComponent basketItems={basketItems} total={total} />
            </Provider>
        );

        const rows = getAllByRole('row');
        expect(rows.length).toBe(basketItems.length + 2);
    });

    it('renders table with correct headers', () => {
        const total = basketItems.reduce((acc, item) => acc + item.price * item.quantity, 0);
        const { getByText } = render(
            <Provider store={store}>
                <BasketTableComponent basketItems={basketItems} total={total} />
            </Provider>
        );
    
        expect(getByText('Title')).toBeInTheDocument();
        expect(getByText('Image')).toBeInTheDocument();
        expect(getByText('Decrease')).toBeInTheDocument();
        expect(getByText('Amount')).toBeInTheDocument();
        expect(getByText('Increase')).toBeInTheDocument();
        expect(getByText('Price')).toBeInTheDocument();
        expect(getByText('Remaining stock')).toBeInTheDocument();
        expect(getByText('Sum')).toBeInTheDocument();
    });

    it('renders table with correct basket item details', () => {
        const total = basketItems.reduce((acc, item) => acc + item.price * item.amount, 0);
        const { getByText } = render(
            <Provider store={store}>
                <BasketTableComponent basketItems={basketItems} total={total} />
            </Provider>
        );

        expect(getByText('Item 1')).toBeInTheDocument();
        expect(getByText('Item 2')).toBeInTheDocument();    
        expect(getByText('2')).toBeInTheDocument();
        expect(getByText('1')).toBeInTheDocument();
        expect(getByText('8')).toBeInTheDocument();
        expect(getByText('5')).toBeInTheDocument();
        expect(getByText(`${total.toFixed(2)} $`)).toBeInTheDocument();
    });

    it('renders table with empty basket', () => {
        const { getByText } = render(
            <Provider store={store}>
                <BasketTableComponent basketItems={[]} total={0} />
            </Provider>
        );
    
        expect(getByText('Total')).toBeInTheDocument();
        expect(getByText('0.00 $')).toBeInTheDocument();
    });

    it('updates table when basket items change', () => {
        const total = basketItems.reduce((acc, item) => acc + item.price * item.quantity, 0);
        const { getByText, rerender } = render(
            <Provider store={store}>
                <BasketTableComponent basketItems={basketItems} total={total} />
            </Provider>
        );
    
        expect(getByText('Item 1')).toBeInTheDocument();
        expect(getByText('Item 2')).toBeInTheDocument();
    
        const updatedBasketItems = [
            { id: 4, title: 'Item 4', image: 'image4.jpg', price: 20, amount: 3, quantity: 10 },
            { id: 5, title: 'Item 5', image: 'image5.jpg', price: 25, amount: 1, quantity: 16 }
        ];
        const updatedTotal = updatedBasketItems.reduce((acc, item) => acc + item.price * item.quantity, 0);
        rerender(
            <Provider store={store}>
                <BasketTableComponent basketItems={updatedBasketItems} total={updatedTotal} />
            </Provider>
        );

        expect(getByText('Item 4')).toBeInTheDocument();
        expect(getByText('Item 5')).toBeInTheDocument();
        expect(getByText(`${updatedTotal.toFixed(2)} $`)).toBeInTheDocument();
    });

    it('renders total with dollar sign', () => {
        const total = basketItems.reduce((acc, item) => acc + item.price * item.quantity, 0);
        const { getByText } = render(
            <Provider store={store}>
                <BasketTableComponent basketItems={basketItems} total={total} />
            </Provider>
        );

        expect(getByText(`${total.toFixed(2)} $`)).toBeInTheDocument();
    });
});
