import React from 'react';
import { Provider } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import BasketItemComponent from '@components/BasketComponents/BasketItemComponent';

const mockStore = configureMockStore();

describe('BasketItemComponent', () => {
    it('renders correctly with given props', () => {
        const store = mockStore();
        const item = {
            id: 1,
            title: 'Test Item',
            image: 'test.jpg',
            price: 10,
            amount: 2,
            quantity: 5,
        };
        const { getByText, getByAltText } = render(
            <Provider store={store}>
                <BasketItemComponent key={item.id} item={item} {...item} />
            </Provider>
        );

        expect(getByText('Test Item')).toBeInTheDocument();
        expect(getByAltText('food')).toBeInTheDocument();
        expect(getByText('2')).toBeInTheDocument();
        expect(getByText('10 $')).toBeInTheDocument();
        expect(getByText('5')).toBeInTheDocument();
        expect(getByText('20 $')).toBeInTheDocument();
    });

    it('dispatches handleDecrement action when decrement button is clicked', () => {
        const store = mockStore();
        const item = {
            id: 1,
            title: 'Test Item',
            image: 'test.jpg',
            price: 10,
            amount: 2,
            quantity: 5,
        };
        const { getByLabelText } = render(
            <Provider store={store}>
                <BasketItemComponent key={item.id} item={item} {...item} />
            </Provider>
        );

        const decrementButton = getByLabelText('decrement');
        fireEvent.click(decrementButton);

        expect(store.getActions()).toEqual([
            { type: 'DECREMENT_AMOUNT', payload: 1 }
        ]);
    });

    it('dispatches handleIncrement action when increment button is clicked', () => {
        const store = mockStore();
        const item = {
            id: 1,
            title: 'Test Item',
            image: 'test.jpg',
            price: 10,
            amount: 2,
            quantity: 5,
        };
        const { getByLabelText } = render(
            <Provider store={store}>
                <BasketItemComponent key={item.id} item={item} {...item} />
            </Provider>
        );

        const incrementButton = getByLabelText('increment');
        fireEvent.click(incrementButton);
        
        expect(store.getActions()).toEqual([
            { type: 'INCREMENT_AMOUNT', payload: 1 }
        ]);
    });

    it('dispatches handleRemove action when delete button is clicked', () => {
        const store = mockStore();
        const item = {
            id: 1,
            title: 'Test Item',
            image: 'test.jpg',
            price: 10,
            amount: 2,
            quantity: 5,
        };
        const { getByLabelText } = render(
            <Provider store={store}>
                <BasketItemComponent key={item.id} item={item} {...item} />
            </Provider>
        );

        const incrementButton = getByLabelText('delete-btn');
        fireEvent.click(incrementButton);

        expect(store.getActions()).toEqual([
            { type: 'REMOVE_FROM_BASKET', payload: 1 }
        ]);
    });
});
