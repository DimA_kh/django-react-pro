import React from 'react';
import { Provider } from 'react-redux';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import BasketIcon from '@components/BasketComponents/BasketIcon';

const mockStore = configureStore([]);

describe('BasketIcon Component', () => {
    it('should render without crashing', () => {
        const store = mockStore({
            basketReducer: {
                basketItems: [],
            },
        });
        const { container } = render(
            <Provider store={store}>
                <BasketIcon onToggleShow={false} />
            </Provider>
        );

        expect(container).toBeInTheDocument();
    });

    it('should pass props correctly', () => {
        const store = mockStore({
            basketReducer: {
                basketItems: [{ id: 1, name: 'Pizza' }],
            },
        });
        const { getByText } = render(
            <Provider store={store}>
                <BasketIcon onToggleShow={false} />
            </Provider>
        );

        expect(getByText('BASKET')).toBeInTheDocument();
        expect(getByText('1')).toBeInTheDocument();
    });

    it('should call onToggleShow when clicked', () => {
        const store = mockStore({
            basketReducer: {
                basketItems: [],
            },
        });
        const onToggleShow = jest.fn();
        const { getByText } = render(
            <Provider store={store}>
                <BasketIcon onToggleShow={onToggleShow} />
            </Provider>
        );
        const basketButton = getByText('BASKET');
        fireEvent.click(basketButton);

        expect(onToggleShow).toHaveBeenCalled();
    });
});
