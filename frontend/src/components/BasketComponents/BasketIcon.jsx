import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import PropTypes from 'prop-types';
import '@components/BasketComponents/BasketIcon.css';

function BasketIcon(props) {
    return (
        <div
            className="basket-icon"
            onClick={props.onToggleShow}
            onKeyDown={props.onToggleShow}
            role="button"
            tabIndex={0}
        >
            <Button className="basket-button">
                {props.basketItems.length ? <span className="basket-count">{props.basketItems.length}</span> : null}
                BASKET
            </Button>
        </div>
    );
}

BasketIcon.propTypes = {
    onToggleShow: PropTypes.func.isRequired,
    basketItems: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
    basketItems: state.basketReducer.basketItems,
});

export default connect(mapStateToProps)(BasketIcon);
