import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Loader from '@components/CommonComponents/loader'
import Header from '@components/CommonComponents/Header';
import Footer from '@components/CommonComponents/Footer';
import { AuthorizedUserRoute, UnauthorizedUserRoute } from '@services/userRoute';

const MainPage = React.lazy(() => import('@components/MainPage'));
const Login = React.lazy(() => import('@components/CommonComponents/login'));
const PasswordResetRequest = React.lazy(() => import('@components/CommonComponents/PasswordResetRequest'));
const ResetPassword = React.lazy(() => import('@components/CommonComponents/ResetPassword'));
const Register = React.lazy(() => import('@components/CommonComponents/register'));
const Profile = React.lazy(() => import('@components/CommonComponents/profile'));
const CheckoutPage = React.lazy(() => import('@components/OrderComponents/CheckoutPage'));
const OrderPage = React.lazy(() => import('@components/OrderComponents/OrderPage'));
const UserOrders = React.lazy(() => import('@components/OrderComponents/UserOrders'));

function App() {
    return (
        <BrowserRouter>
            <Header />
            <React.Suspense fallback={<Loader />}>
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="/login" element={<AuthorizedUserRoute><Login /></AuthorizedUserRoute>} />
                    <Route path="/password-reset" element={<AuthorizedUserRoute><PasswordResetRequest /></AuthorizedUserRoute>} />
                    <Route path="/password-reset/:uid/:token" element={<AuthorizedUserRoute><ResetPassword /></AuthorizedUserRoute>} />
                    <Route path="/register" element={<AuthorizedUserRoute><Register /></AuthorizedUserRoute>} />
                    <Route path="/profile" element={<UnauthorizedUserRoute><Profile /></UnauthorizedUserRoute>} />
                    <Route path="/checkout" element={<UnauthorizedUserRoute><CheckoutPage /></UnauthorizedUserRoute>} />
                    <Route path="/order" element={<UnauthorizedUserRoute><UserOrders /></UnauthorizedUserRoute>} />
                    <Route path="/order/:orderID" element={<UnauthorizedUserRoute><OrderPage /></UnauthorizedUserRoute>} />
                </Routes>
            </React.Suspense>
            <Footer />
        </BrowserRouter>
    );
}

export default App;
