import React from 'react';
import { render } from '@testing-library/react';
import App from '@/App';

jest.mock('@components/MainPage', () => {
    return function MainPage() {
        return <div>MainPage component</div>;
    }
});

jest.mock('@components/CommonComponents/register', () => {
    return function Register() {
        return <div>Register component</div>;
    }
});

jest.mock('@components/CommonComponents/profile', () => {
    return function Profile() {
        return <div>Profile component</div>;
    }
});

jest.mock('@components/OrderComponents/CheckoutPage', () => {
    return function CheckoutPage() {
        return <div>CheckoutPage component</div>;
    }
});

jest.mock('@components/OrderComponents/OrderPage', () => {
    return function OrderPage() {
        return <div>OrderPage component</div>;
    }
});

jest.mock('@components/OrderComponents/UserOrders', () => {
    return function UserOrders() {
        return <div>UserOrders component</div>;
    }
});

jest.mock('@components/ProductComponents/ProductSearch', () => {
    return function ProductSearch() {
        return <div data-testid="mock-product-search">ProductSearch component</div>;
    };
});

test('renders App without crashing', () => {
    const { getByTestId } = render(<App />);
    const headerElement = getByTestId('test-header');
    const footerElement = getByTestId('test-footer');
    expect(headerElement).toBeInTheDocument();
    expect(footerElement).toBeInTheDocument();
});
